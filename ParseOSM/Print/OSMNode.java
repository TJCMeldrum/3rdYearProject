class OSMNode {

    private double lon;
    private double lat;
    private long num;

    public OSMNode(long num, double lat, double lon) {
        this.lon = lon;
        this.lat = lat;
        this.num = num;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public long getNum() {
        return num;
    }

}
