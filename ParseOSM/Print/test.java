import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.io.*;
import java.util.Random;
import java.util.ArrayList;

class test extends JApplet {
    final static int maxCharHeight = 15;
    final static int minFontSize = 6;

    final static Color bg = Color.white;
    final static Color fg = Color.black;
    final static Color path = Color.red;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f,
                                                      BasicStroke.CAP_BUTT,
                                                      BasicStroke.JOIN_MITER,
                                                      10.0f, dash1, 0.0f);
    Dimension totalSize;
    FontMetrics fontMetrics;

    public static void main(String[] args) {
        JFrame f = new JFrame("test");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });
        JApplet applet = new test();
        f.getContentPane().add("Center", applet);
        applet.init();
        f.pack();
        f.setSize(new Dimension(1000,1000));
        f.setVisible(true);
    }

    public void init() {
        //Initialize drawing colors
        setBackground(bg);
        setForeground(fg);
    }

    public void paint (Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Dimension d = getSize();
        int gridWidth = d.width;
        int gridHeight = d.height;
        int x = 0;
        int y = 0;
        g2.setBackground(bg);


        g2.setPaint(bg);
        g2.fill (new Rectangle2D.Double(x, y, d.width, d.height));
        g2.setPaint(fg);
        // g2.draw(new Line2D.Double(x/2, y/2, x +  d.width, y + d.height));

        ParseOSM parser = new ParseOSM();
        try {

            Graph graph = parser.parseGraph("Oxford.txt");

            double north = 0;
            double west = 0;
            double south = 0;
            double east = 0;
            Double[] point2 = graph.getVertexLoc(0);
            Double[] point = graph.getVertexLoc(0);
            north = point[0];
            south = point[0];
            west = point[1];
            east = point[1];

            for (int i = 1; i < graph.size() ; i++) {
                point = graph.getVertexLoc(i);
                if( north  < point[0]) {
                    north = point[0];
                }
                if( south > point[0]) {
                    south = point[0];
                }
                if( west > point[1]) {
                    west = point[1];
                }
                if( east < point[1]) {
                    east = point[1];
                }
            }


            for (int j = 0; j < graph.size() ; j++ ) {
            // for (int j = 0; j < 100 ; j++ ) {
                point = graph.getVertexLoc(j);
                // System.out.println(point[0]);
                // System.out.println(point[1]);
                // System.out.println(north - south);
                // System.out.println( west - east );
                double y1 = d.height - ((point[0] - south) / (north - south)) * ((d.height) );
                double x1 = ((point[1] - west) / Math.abs(( west - east )) * ((d.width)));

                // System.out.println(x1);
                // System.out.println(y1);

                g2.draw(new Line2D.Double(x1, y1, x1, y1));

            }
            g2.setPaint(path);


            Random rand = new Random();
            // int s = rand.nextInt(graph.size());
            int s = 5429;
            // int t = rand.nextInt(graph.size());
            int t = 8021;

            ArrayList<Integer> path = graph.djkAlgo(s, t);
            for (int k = 0; k < path.size() - 1; k++ ) {
                point = graph.getVertexLoc(path.get(k));
                point2 = graph.getVertexLoc(path.get(k+1));
                double y1 = d.height - ((point[0] - south) / (north - south)) * ((d.height) );
                double y2 = d.height - ((point2[0] - south) / (north - south)) * ((d.height) );
                double x1 = ((point[1] - west) / Math.abs(( west - east )) * ((d.width)));
                double x2 = ((point2[1] - west) / Math.abs(( west - east )) * ((d.width)));

                // System.out.println(x1);
                // System.out.println(y1);

                g2.draw(new Line2D.Double(x1, y1, x2, y2));
            }


            System.out.println("fin me");

    } catch (IOException e) {
        System.out.println("No file");
        return;
    }


    }

}
