import java.util.ArrayList;
class OSMNodeMap {

    private ArrayList<OSMNode> nodes;
    private ArrayList<Long> ref;

    public OSMNodeMap() {
        nodes = new ArrayList<OSMNode>();
        ref = new ArrayList<Long>();
    }

    public void addNode(Long num, double lat, double lon) {
        nodes.add(new OSMNode(num, lat, lon));
        ref.add(num);
    }

    public void addNode(OSMNode n) {
        nodes.add(n);
        ref.add(n.getNum());
    }

    public OSMNode getNode(Long num) {
        return nodes.get(ref.indexOf(num));
    }

    public OSMNode getNodeRef(int ref) {
        return nodes.get(ref);
    }

    public int getRef(Long num) {
        return ref.indexOf(num);
    }



}
