import java.util.ArrayList;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class Graph {

    //Number of nodes in the graph
    private int size;
    //Number of edges in the graph
    private int edgeNum;
    //Arraylist if linked list. Edges of node x is stored at position x on the array lists.
    private ArrayList<LinkedListHead> adjList;


    public Graph(int size) {
        this.size = size;
        adjList = new ArrayList<LinkedListHead>(size);
        for (int i = 0; i < size ; i++ ) {
            LinkedListHead x = new LinkedListHead();
            adjList.add(x);
        }
        edgeNum = 0;
    }

    /**
     * Returns the location of a node v as its longtude and latitude in a arry of doubles of size 2.
     * It gets the LinkedListHead and finds the location soted in there.
     * @param v the node to find the location of
     * @return an array containg the location
     */
    public Double[] getVertexLoc(int v) {
        LinkedListHead vNode = adjList.get(v);
        Double[] ll = { vNode.lat(), vNode.lon() };
        return ll;
    }

    /**
     * Adds a edge to the graph bettween u and v of a specified int.
     * If the edge exists it prints out message to the consol and returns.
     * If one of the nodes is not in the graph the exceoption is court the nodes printed to the consol as
     * well as a error message.
     * @param v node to add the edge too
     * @param u node to add the edge too
     * @param weight weight of the edge that is going to be added
     */
    public void addEdge(int v, int u, int weight) {
        try{
            if(edgeExist(v,u)) {
                System.out.println("Edge exists");
                return;
            }
            adjList.get(v).add(u, weight);
            adjList.get(u).add(v, weight);
            edgeNum++;
        } catch (IndexOutOfBoundsException e ) {
            System.out.println("v" + v);
            System.out.println("u" + u);
            System.out.println("Invalid Edge");
        }
    }

    /**
     * Adds a edge to the graph bettween u and v of weight of the distance bettween the 2 points in terms of longtude and latitude.
     * see https://www.movable-type.co.uk/scripts/latlong.html for deatails of calulations
     * If one of the nodes is not in the graph the exceoption is court the nodes printed to the consol as
     * well as a error message.
     * @param v node to add the edge too
     * @param u node to add the edge too
     * @return returns the weight of the edge as calulated
     */
    public double addEdge(int v, int u) {
        try {
            // LinkedList e = getEdge(v, u);
            if(edgeExist(v,u)) {
                // System.out.println("HI");
                return -1;
            }
            double R = 6371e3;
            double vlon = Math.toRadians(adjList.get(v).lon());
            double vlat = Math.toRadians(adjList.get(v).lat());
            double ulon =Math.toRadians(adjList.get(u).lon());
            double ulat = Math.toRadians(adjList.get(u).lat());
            double changeLat = (vlat - ulat);
            double changeLon = (vlon - ulon);

            double a = Math.sin(changeLat/2) * Math.sin(changeLat/2) + Math.cos(ulat) * Math.cos(vlat) * Math.sin(changeLon/2) * Math.sin(changeLon/2);

            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));


            double weight = R * c;

            adjList.get(v).add(u, weight);
            adjList.get(u).add(v, weight);
            edgeNum++;
            return weight;
        } catch (IndexOutOfBoundsException e ) {
            System.out.println("v" + v);
            System.out.println("u" + u);
            System.out.println("Invalid Edge");
            return -1;
        }
    }


    /**
     * Adds a edge to the graph bettween u and v of a specified double.
     * If the edge exists it prints out message to the consol and returns.
     * If one of the nodes is not in the graph the exceoption is court the nodes printed to the consol as
     * well as a error message.
     * @param v node to add the edge too
     * @param u node to add the edge too
     * @param weight weight of the edge that is going to be added
     */
    public void addEdge(int v, int u, double weight) {
        try{
            if(edgeExist(v,u)) {
                System.out.println("Edge exists");
                return;
            }
            adjList.get(v).add(u, weight);
            adjList.get(u).add(v, weight);
            edgeNum++;
        } catch (IndexOutOfBoundsException e ) {
            System.out.println("v" + v);
            System.out.println("u" + u);
            System.out.println("Invalid Edge");
        }
    }


    /**
     * Get the edge in the form a LinkedList element specifyed by 2 vertices. If there is no edge bettween the 2 nodes
     * it returns the end node which has weight of -1
     * @param v node
     * @param u node
     * @return returns the weight of the edge
     */
    public LinkedList getEdge (int v, int u) {
        LinkedList edge = getEdges(v).getHead();
        while (!edge.isEnd()) {
            if (edge.getVertex() == u) {
                return edge;
            }
            edge = edge.getNext();
        }
        return edge;
    }

    /**
     * Checks for the existance of a edge bettween 2 edges true is there is an edge false otherwise
     * @param v node
     * @param u node
     * @return returns true if edge exists false otehrwise
     */
    public boolean edgeExist(int v, int u) {
        LinkedList edge  = getEdges(v).getHead();
        while (!edge.isEnd()) {
            if(edge.getVertex() == u) return true;
            edge = edge.getNext();
        }
        return false;
    }

    /**
     * Adds a vertex at point lat, lon. The number of the node will be the the index of the adjlist.
     * @param lat the latitude of the node`
     * @param lon the lonitude of the node`
     * @return returns the new size of the graph
     */
    public int addVertex (double lat, double lon) {
        adjList.add(new LinkedListHead(lon, lat));
        size++;
        return adjList.size() - 1;
    }

    /**
     * Adds a vertex at point 0, 0. The number of the node will be the the index of the adjlist.
     * @return returns the new size of the graph
     */
    public int addVertex () {
        adjList.add(new LinkedListHead(0, 0));
        size++;
        return adjList.size() - 1;
    }

    /**
     * Gets the LinkedListHead that store the edges of the node v
     * @param v the node to get the edges of
     * @return LinkedListHead storeing edges of v
     */
    public LinkedListHead getEdges(int v) {
        return adjList.get(v);
    }

    /**
     * Gets the degree of a node in the graph by counting the elements in the list
     * @param v the node to get degree of
     * @return returns the degree of v
     */
    public int getDregree(int v) {
        return adjList.get(v).length();
    }

    /**
     * Gets the nnumber of nodes in the graph
     * @return returns the number of nodes
     */
    public int size() {
        return size;
    }

    public int getEdgeNum() {
        return edgeNum;
    }

    /**
     * This method finds the shortest path between 2 points. It dose this using the djikstras algoritum with an fib heap.
     * it prints the distacnce to the consol then return the path.
     * @param start the start node
     * @param end the end node
     * @return an arraylist detailing the shortest path
     * @see sameSubGraph()
     * @see relaxEdge()
     */
    public ArrayList<Integer> djkAlgo(int start, int end) {

        //Checks if the start is equal to the end if so distace is 0 and returns a empty list
        if(start == end) {
            System.out.println(0);
            return new ArrayList<Integer>();
        }
        // Checks if nodes are in same sub graph @see sameSubGraph for deatails.
        // If not in the same graph returns no path as there is no path
        else if (!sameSubGraph(start, end)) {
            System.out.println("No Path");
            return new ArrayList<Integer>();
        }
        // Intalised arrys to store distance, path nodes and queue for chosing nodes
        double[] distance = new double[size];
        int[] previous = new int[size];
        PrioQueue queue = new PrioQueue(size);
        // Sets all nodes distance to max value then inserts it in the queue then sets previous node in the shortest path to -1
        for (int i = 0; i < size ; i++ ) {
            distance[i] = Integer.MAX_VALUE;
            queue.insert(i, distance[i]);
            previous[i] = -1;
        }

        // Intalising starting values
        distance[start] = 0;
        queue.decreseKey(start, 0);
        previous[start] = start;

        // Gets minimum node (start)
        int currentVertex = queue.extractMin();

        // Repeats untill the current node is the end node
        while (currentVertex != end) {

            //If the previous vertex is -1 then there is no path from start to currentVertex so the algoritum must be at end
            if (previous[currentVertex] == -1) {
                break;
            }

            // gets list of edges of currentVertex
            LinkedList edge = adjList.get(currentVertex).getHead();

            // Relaxs all edges connected to currentVertex
            while (!(edge.isEnd())) {
                relaxEdge(currentVertex, edge.getVertex(), edge.getWeight(), distance, previous, queue);
                edge = edge.getNext();
            }
            // Gets new node to consider
            currentVertex = queue.extractMin();
        }

        //Gets the path using the previous array
        Integer pathNode = end;
        ArrayList<Integer> path = new ArrayList<Integer>();
        do {
            path.add(pathNode);
            pathNode = previous[pathNode];
        } while (pathNode != start);
        path.add(pathNode);
        System.out.println(distance[end]);

        return path;
    }

    /**
    * Same as djkAlgo but returns distace insted of path.
    * @param start the start node
    * @param end the end node
    * @return an arraylist detailing the shortest path
    * @see sameSubGraph()
    * @see relaxEdge()
    */
    public double djkAlgoDist(int start, int end) {
        if(start == end) {
            System.out.println(0);
            return 0;
        }
        else if (!sameSubGraph(start, end)) {
            return Double.MAX_VALUE;
        }
        double[] distance = new double[size];
        int[] previous = new int[size];
        PrioQueue queue = new PrioQueue(size);
        for (int i = 0; i < size ; i++ ) {
            distance[i] = Integer.MAX_VALUE;
            queue.insert(i, distance[i]);
            previous[i] = -1;
        }

        distance[start] = 0;
        queue.decreseKey(start, 0);
        previous[start] = start;

        int currentVertex = queue.extractMin();

        while (currentVertex != end) {
            if (previous[currentVertex] == -1) {
                break;
            }
            LinkedList edge = adjList.get(currentVertex).getHead();
            while (!(edge.isEnd())) {
                relaxEdge(currentVertex, edge.getVertex(), edge.getWeight(), distance, previous, queue);
                edge = edge.getNext();
            }
            currentVertex = queue.extractMin();
        }

        return distance[end];
    }


    public double biDjkAlgoDist(int start, int end) {
        if(start == end) {
            System.out.println(0);
            return 0;
        }
        else if (!sameSubGraph(start, end)) {
            return Double.MAX_VALUE;
        }
        double[] distanceS = new double[size];
        double[] distanceT = new double[size];
        int[] previousS = new int[size];
        int[] previousT = new int[size];
        PrioQueue queueS = new PrioQueue(size);
        PrioQueue queueT = new PrioQueue(size);

        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int i = 0; i < size ; i++ ) {
            distanceS[i] = Integer.MAX_VALUE;
            distanceT[i] = Integer.MAX_VALUE;
            queueS.insert(i, distanceS[i]);
            queueT.insert(i, distanceT[i]);
            previousS[i] = -1;
            previousT[i] = -1;
            done.add(false);
        }

        distanceS[start] = 0;
        queueS.decreseKey(start, 0);
        previousS[start] = start;

        distanceT[end] = 0;
        queueT.decreseKey(end, 0);
        previousT[end] = end;

        int currentVertexS = queueS.extractMin();
        int currentVertexT = queueT.extractMin();

        // done.set(start, true);
        // done.set(end, true);

        while (currentVertexS != end) {
            if (previousS[currentVertexS] == -1) {
                break;
            }

            if (previousT[currentVertexT] == -1) {
                break;
            }


            LinkedList edgeS = adjList.get(currentVertexS).getHead();
            while (!(edgeS.isEnd())) {
                relaxEdge(currentVertexS, edgeS.getVertex(), edgeS.getWeight(), distanceS, previousS, queueS);
                edgeS = edgeS.getNext();
            }
            if (done.get(currentVertexS)) {
                break;
            }
            done.set(currentVertexS, true);

            currentVertexS = queueS.extractMin();

            LinkedList edgeT = adjList.get(currentVertexT).getHead();
            while (!(edgeT.isEnd())) {
                relaxEdge(currentVertexT, edgeT.getVertex(), edgeT.getWeight(), distanceT, previousT, queueT);
                edgeT = edgeT.getNext();
            }
            if (done.get(currentVertexT)) {
                break;
            }
            done.set(currentVertexT, true);
            currentVertexT = queueT.extractMin();

        }

        double distance = Double.MAX_VALUE;
        for (int i = 0; i < size ; i++ ) {
            if(distance > distanceS[i] + distanceT[i]) {
                distance = distanceS[i] + distanceT[i];
            }
        }

        return distance;
    }



    /**
     * Perfroms the relax edge operation of djikstras algoritum. Takes 2 nodes and checks the distace from start -> u is
     * bigger than the distance from start -> v -> u. This method works as java pass by object but objects in the heap and edited in the heap
     * so when relexedge chnages object values they change in djkAlgo as well
     * @param v current node
     * @param u the end node
     * @param weight the edge weight of (u,v)
     * @param distace the distace array
     * @param previous the path array
     * @param q is the priority queue that needs changing
     */
    private void relaxEdge(int v, int u, double weight, double[] distance, int[] previous, PrioQueue q) {
        if (distance[u] >= distance[v] + weight) {
            distance[u] = distance[v] + weight;
            previous[u] = v;
            q.decreseKey(u, distance[u]);
        }
    }

    /**
     * Test method smae as size but using a diffent mesure that should be the same
     * @return returns size of the graph
     */
    public int getSize() {
        return adjList.size();
    }

    /**
     * Removes a specifyed node of degree 2 from the graph and adds a new edge between the nodes of correct weight.
     * Creats a minor of the graph
     * @param n the node to remove
     */
    public void removeNode(int n) {
        LinkedListHead node = adjList.get(n);
        LinkedList edgev = node.getHead();
        LinkedList edgeu = node.getHead().getNext();
        int vNum = edgev.getVertex();
        int uNum = edgeu.getVertex();
        removeEdge(vNum, n);
        removeEdge(uNum, n);
        addEdge(vNum, uNum, edgeu.getWeight() + edgev.getWeight());
    }


    public void toFile() throws IOException {
        PrintWriter writer = new PrintWriter("Graph.txt", "UTF-8");
        for (int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            // writer.println("N:" + i + " Lat:" + node.lat() + " Lon:" + node.lon() + " OSM:" + node.OSM());
            writer.println("N:" + i + " Lat:" + node.lat() + " Lon:" + node.lon());

        }
        for(int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            LinkedList edge = node.getHead();
            while(!edge.isEnd()) {
                if(edge.getVertex() < i) {
                    writer.println("u:" + i + " v:" + edge.getVertex() + " w:" + edge.getWeight());
                }
                edge = edge.getNext();
            }
        }
        writer.close();

    }

    public void toFile(String fileName) throws IOException {
        PrintWriter writer = new PrintWriter(fileName + ".txt", "UTF-8");
        for (int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            // writer.println("N:" + i + " Lat:" + node.lat() + " Lon:" + node.lon() + " OSM:" + node.OSM());
            writer.println("N:" + i + " Lat:" + node.lat() + " Lon:" + node.lon());
        }
        for(int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            LinkedList edge = node.getHead();
            while(!edge.isEnd()) {
                if(edge.getVertex() < i) {
                    writer.println("u:" + i + " v:" + edge.getVertex() + " w:" + edge.getWeight());
                }
                edge = edge.getNext();
            }
        }
        writer.close();

    }

    /**
     * Performs a deep copy of the graph and returns it assumes no self edges
     * @return an copy of the graph
     */
    public Graph copy () {
        Graph g = new Graph(0);
        //Add all nodes in the same location
        for (int i = 0;  i < adjList.size() ; i++ ) {
            LinkedListHead node = adjList.get(i);
            g.addVertex(node.lat(),node.lon());
        }

        // Adds all edged to the graph every edge considered twice
        for(int i = 0;  i < adjList.size() ; i++ ) {
            LinkedListHead node = adjList.get(i);
            LinkedList edge = node.getHead();
            while(!edge.isEnd()) {
                // Check only add egde when i is the smaller number prevent repitions
                if (i < edge.getVertex()) {
                    g.addEdge(i, edge.getVertex(), edge.getWeight());
                }
                edge = edge.getNext();
            }
        }
        return g;
    }

    /**
     * This method upperbounds the treewidth using the minimum degree huristic. Each time it checks for
     * the node in the the graph and elimanted. The prosess is then repeats untill every node is elimated
     * from the graph. The bound is the maxuim degree of a node that is elimanted at time of elimination.
     * @return Returns a unpperbound on treewidth
     * @see eliminate
     */
    public int treeWidthUMinDegree() {
        // Sets up possible bound
        int treewidthBound = -1;
        // Creates copy of graoh so no changes to the graph is made
        Graph g = copy();
        // done specifyes if a node has been elimated or not
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        // Sets elimated to false as no nodes are yet elimated
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        // Remove all degree nodes of degree 2 since they have no real effect on treewidth if there are nodes
        // with a higher degree
        g.removeDeg2(done);

        // If there are any nodes with degree 0 it is irrelevant
        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        // Elimates nodes one by one
        for (int i = 0; i < size; i++ ) {

            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);

            // Gets the current node with minimum degree that has not yet eleminated
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // Choses node based on degree
                if ( !done.get(j) && ( min == -1 || (g.getDregree(min) > g.getDregree(j)) )) min = j;
            }

            // If min is -1 then there are no nodes left not yet eleminated
            if (min == -1) {
                break;
            }

            // Checks if the degree of the node is equal to the size of the clique created when elimated
            // and max clique size in a triangulation is a upperbound of treewidth
            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);

            // Elimates the current lowest degree node
            g.eliminate(min);

            // Sets current min to done
            done.set(min, true);

        }
        return treewidthBound;
    }

    /**
     * This method upperbounds the treewidth using the minimum fillin huristic. Same as treeWidthUMinDegree
     * apart from using fillIn to chose nodes insted of degree
     * @return Returns a unpperbound on treewidth
     * @see eliminate
     * @see fillIn
     */
    public int treeWidthUMinFillin()  {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        for (int i = 0; i < size; i++ ) {
            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // Choses node beased on treewidth
                if ( !done.get(j) && ( min == -1 || (g.fillIn(min) > g.fillIn(j)) )) min = j;
            }

            if (min == -1) {
                break;
            }

            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
            g.eliminate(min);
            done.set(min, true);

        }
        return treewidthBound;
    }

    /**
     * This method upperbounds the treewidth using the minimum fillin + degree huristic. Same as treeWidthUMinDegree
     * apart from using fillin + degree to chose nodes insted of degree
     * @return Returns a unpperbound on treewidth
     * @see eliminate
     * @see degfillIn
     */
    public int treeWidthUMinDegreeFillin() {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        int counter = 0;
        for (int i = 0; i < size; i++) {
            if (done.get(i)) {
                counter++;
            }
        }

        for (int i = 0; i < size; i++ ) {
            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);

            int min = -1;

            for (int j = 0; j < size ; j++ ) {
                // Choses node based on degree + fillIn huristic
                if ( !done.get(j) && ( min == -1 || (g.getDegFillin(min) > g.getDegFillin(j))  )) min = j;
            }
            if(min == -1) {
                break;
            }

            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);

            g.eliminate(min);

            done.set(min, true);
        }
        return treewidthBound;
    }

    /**
     * This method upperbounds the treewidth using the minimum fillin + degree huristic. Same as treeWidthUMinDegreeFillin.
     * It is a legacy method that dose not remove nodes of degree 2.
     * @return Returns a unpperbound on treewidth
     * @see eliminate
     * @see degfillIn
     */
    public int treeWidthUMinDegreeFillinND2() {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        // g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        int counter = 0;
        for (int i = 0; i < size; i++) {
            if (done.get(i)) {
                counter++;
            }
        }

        for (int i = 0; i < size; i++ ) {
            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);

            int min = -1;

            for (int j = 0; j < size ; j++ ) {
                if ( !done.get(j) && ( min == -1 || (g.getDegFillin(min) > g.getDegFillin(j))  )) min = j;
            }
            if(min == -1) {
                break;
            }

            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);

            g.eliminate(min);

            done.set(min, true);
        }
        return treewidthBound;
    }

    public int treeWidthUMinDegreeND2() {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        // g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        int counter = 0;
        for (int i = 0; i < size; i++) {
            if (done.get(i)) {
                counter++;
            }
        }

        for (int i = 0; i < size; i++ ) {
            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);

            int min = -1;

            for (int j = 0; j < size ; j++ ) {
                if ( !done.get(j) && ( min == -1 || (g.getDregree(min) > g.getDregree(j))  )) min = j;
            }
            if(min == -1) {
                break;
            }

            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);

            g.eliminate(min);

            done.set(min, true);
        }
        return treewidthBound;
    }

    public int treeWidthUMinFillinND2() {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        // g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        int counter = 0;
        for (int i = 0; i < size; i++) {
            if (done.get(i)) {
                counter++;
            }
        }

        for (int i = 0; i < size; i++ ) {
            if (i % 1000 == 0) System.out.println("Iteration: " + i + " TW: " + treewidthBound);

            int min = -1;

            for (int j = 0; j < size ; j++ ) {
                if ( !done.get(j) && ( min == -1 || (g.fillIn(min) > g.fillIn(j))  )) min = j;
            }
            if(min == -1) {
                break;
            }

            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);

            g.eliminate(min);

            done.set(min, true);
        }
        return treewidthBound;
    }

    public int treeWidthUMinDegree(double bufferSize) {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        for (int i = 0; i < Math.ceil(size / bufferSize); i++ ) {
            BinHeap buffer = new BinHeap<Integer>();
            if (i % (10) == 0 )System.out.println("Iteration: " + i + " TW: " + treewidthBound);
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // if ( done.indexOf(j) == -1 && ( min == -1 || (g.getDregree(min) > g.getDregree(j)) )) min = j;
                if ( !done.get(j) ) buffer.insert(new KVP<Integer>(g.getDregree(j), j));
            }

            if(buffer.size() == 0) break;

            int couter = 0;
            while(couter++ < bufferSize && buffer.size() != 0) {
                min = (int) buffer.getMin().getValue();
                if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
                g.eliminate(min);
                done.set(min, true);
            }

        }
        return treewidthBound;
    }

    public int treeWidthUMinFillin(double bufferSize) {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        for (int i = 0; i < Math.ceil(size / bufferSize); i++ ) {
            BinHeap buffer = new BinHeap<Integer>();
            if (i % (10) == 0 )System.out.println("Iteration: " + i + " TW: " + treewidthBound);
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // if ( done.indexOf(j) == -1 && ( min == -1 || (g.getDregree(min) > g.getDregree(j)) )) min = j;
                if ( !done.get(j) ) buffer.insert(new KVP<Integer>(g.fillIn(j), j));
            }

            if(buffer.size() == 0) break;

            int couter = 0;
            while(couter++ < bufferSize && buffer.size() != 0) {
                min = (int) buffer.getMin().getValue();
                if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
                g.eliminate(min);
                done.set(min, true);
            }

        }
        return treewidthBound;
    }

    public int treeWidthUMinDegreeFillin(double bufferSize) {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        g.removeDeg2(done);

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        for (int i = 0; i < Math.ceil(size / bufferSize); i++ ) {
            BinHeap buffer = new BinHeap<Integer>();
            if (i % (10) == 0 )System.out.println("Iteration: " + i + " TW: " + treewidthBound);
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // if ( done.indexOf(j) == -1 && ( min == -1 || (g.getDregree(min) > g.getDregree(j)) )) min = j;
                if ( !done.get(j) ) buffer.insert(new KVP<Integer>(g.getDegFillin(j), j));
            }

            if(buffer.size() == 0) break;

            int couter = 0;
            while(couter++ < bufferSize && buffer.size() != 0) {
                min = (int) buffer.getMin().getValue();
                if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
                g.eliminate(min);
                done.set(min, true);
            }

        }
        return treewidthBound;
    }

    public int getDegFillin(int v) {
        return getDregree(v) + fillIn(v);
    }

    public void eliminate(int v) {

        ArrayList<Integer> nodes = new ArrayList<Integer>();
        LinkedList edge = adjList.get(v).getHead();

        while (!edge.isEnd()) {
            nodes.add(edge.getVertex());
            edge = edge.getNext();
        }

        for (int i = 0; i < nodes.size(); i++) {
            removeEdge( nodes.get(i), v);
        }

        for (int i = 0; i < nodes.size(); i++) {
            for (int j = 0; j < nodes.size(); j++) {
                // if ( nodes.get(i) < nodes.get(j) && !edgeExist(nodes.get(i), nodes.get(j) )) {
                if ( nodes.get(i) < nodes.get(j) ) {
                    addEdge(nodes.get(i), nodes.get(j));
                }
            }
        }
    }

    //Removes edge v from u
    public void removeEdge(int u, int v) {
        removeEdge1side(v, u);
        removeEdge1side(u, v);
        edgeNum--;
    }

    public void removeEdge1side(int u, int v) {
        LinkedListHead uEdgeHead = adjList.get(u);
        LinkedList uEdge = uEdgeHead.getHead();

        if (uEdge.getVertex() == v) {
            uEdgeHead.setHead(uEdge.getNext());
        } else {
            while (!uEdge.isEnd()) {
                if (uEdge.getNext().getVertex() == v) {
                    uEdge.setNext(uEdge.getNext().getNext());
                    break;
                }
                uEdge = uEdge.getNext();
            }
        }
        uEdgeHead.setLength(uEdgeHead.length() - 1);
    }

    /**
     * This method works out if a node is eleminated how many edgeds will bd needed to be added.
     * @param v the node to get the fillIn nuber
     * @return returns fill in number of v
     */
    public int fillIn(int v) {
        ArrayList<Integer> nodes = new ArrayList<Integer>();
        LinkedList edge = adjList.get(v).getHead();
        int fill = 0;

        // Adds all nodes to the list
        while (!edge.isEnd()) {
            nodes.add(edge.getVertex());
            edge = edge.getNext();
        }

        //For each par of nodes it checks if there is a edge between them and counts how many non edges there are.
        for (int i = 0; i < nodes.size(); i++) {
            for (int j = 0; j < nodes.size(); j++) {
                if ( nodes.get(i) < nodes.get(j) && !edgeExist(nodes.get(i), nodes.get(j) )) {
                    fill++;
                }
            }
        }
        return fill;
    }

    public void removeDeg2() {
        for (int i = 0; i < size; i++) {
            if (getDregree(i) == 2) {
                LinkedList e1 = adjList.get(i).getHead();
                LinkedList e2 = e1.getNext();
                if (!edgeExist(e1.getVertex(), e2.getVertex())) {
                    // removeEdge(i, e1.getVertex());
                    // removeEdge(i, e2.getVertex());
                    removeEdge(e1.getVertex(), i);
                    removeEdge(e2.getVertex(), i);
                    addEdge(e1.getVertex(), e2.getVertex());
                }
            }
        }
    }

    public int removeDeg2(ArrayList<Boolean> done) {
        int count = 0;
        for (int i = 0; i < size; i++) {
            if(i % 1000 == 0) System.out.println("Deg 2 " + i);
            if (getDregree(i) == 2 && !done.get(i)) {
                int a = 0;
                double newWeight = 0;
                count++;
                done.set(i, true);
                LinkedList e1 = adjList.get(i).getHead();
                LinkedList e2 = e1.getNext();
                int e1prev = i;
                int e2prev = i;
                newWeight += e1.getWeight() + e2.getWeight();
                while (getDregree(e1.getVertex()) == 2 && !done.get(e1.getVertex())) {
                    a++;

                    LinkedList next = adjList.get(e1.getVertex()).getHead();
                    while(next.getVertex() == e1prev) {
                        next = next.getNext();
                    }

                    e1prev = e1.getVertex();
                    count++;
                    done.set(e1prev, true);
                    e1 = next;
                    newWeight += e1.getWeight();
                }

                while (getDregree(e2.getVertex()) == 2  && !done.get(e2.getVertex())) {
                    a++;
                    LinkedList next = adjList.get(e2.getVertex()).getHead();
                    while(next.getVertex() == e2prev) {
                        next = next.getNext();
                    }
                    e2prev = e2.getVertex();
                    count++;
                    done.set(e2prev, true);
                    e2 = next;
                    newWeight += e2.getWeight();
                }

                if (e1.getVertex() == e2.getVertex()) {
                    done.set(e1prev, false);
                    done.set(e2prev, false);
                    if (edgeExist(e1prev, e2prev)) {
                        count -= 1;
                        continue;
                    }
                    count = count - 2;

                    int e3 = e1.getVertex();

                    e1 = adjList.get(e1prev).getHead();
                    e2 = adjList.get(e2prev).getHead();

                    LinkedList edge = adjList.get(e1prev).getHead();
                    while(!edge.isEnd()) {
                        if(edge.getVertex() == e3) {
                            removeEdge(e1prev, edge.getVertex());
                        } else {
                            newWeight -= edge.getWeight();
                        }
                        edge = edge.getNext();
                    }


                    edge = adjList.get(e2prev).getHead();
                    while(!edge.isEnd()) {
                        if(edge.getVertex() == e3) {
                            removeEdge(e2prev, edge.getVertex());
                        } else {
                            newWeight -= edge.getWeight();
                        }
                        edge = edge.getNext();
                    }

                    addEdge(e2prev, e1prev, newWeight);

                }
                else if (!edgeExist(e1.getVertex(), e2.getVertex())) {

                    removeEdge(e1.getVertex(), e1prev);
                    removeEdge(e2.getVertex(), e2prev);
                    addEdge(e1.getVertex(), e2.getVertex(), newWeight);
                } else if(getEdge(e1.getVertex(), e2.getVertex()).getWeight() > newWeight) {
                    removeEdge(e1.getVertex(), e1prev);
                    removeEdge(e2.getVertex(), e2prev);
                    removeEdge(e1.getVertex(), e2.getVertex());
                    addEdge(e1.getVertex(), e2.getVertex(), newWeight);
                } else {
                    removeEdge(e1.getVertex(), e1prev);
                    removeEdge(e2.getVertex(), e2prev);
                }
            }
        }
        return count;
    }

    public Graph DPCalgo() {
        Graph g = copy();
        g.dpc();
        return g;
    }

    private ArrayList<Integer> dpc() {
        ArrayList<Integer> order = new ArrayList<Integer>();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            // done.add( (getDregree(j) <= 1) ? true:false);
            done.add( false);
        }

        for (int i = 0; i < size; i++) {
            if ( i % 1000 == 0) System.out.println("DPC iteration : " + i);
            int min = -1;
            for (int j = 0; j < size; j++ ) {
                if ( !done.get(j) && ( min == -1 || (getDregree(min) > getDregree(j)) )) min = j;
            }

            LinkedList edge = adjList.get(min).getHead();
            ArrayList<LinkedList> edgeList = new ArrayList<LinkedList>();

            while(!edge.isEnd()) {
                if (!done.get(edge.getVertex())) edgeList.add(edge);
                edge = edge.getNext();
            }

            for (int k = 0; k < edgeList.size() ; k++ ) {
                for(int l = 0; l < edgeList.size(); l++) {
                    int lNum = edgeList.get(l).getVertex();
                    int kNum = edgeList.get(k).getVertex();
                    if(l < k && !edgeExist(edgeList.get(k).getVertex(), lNum) ) {
                        addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                    } else if ( l < k ) {
                        if ( edgeList.get(k).getWeight() + edgeList.get(l).getWeight() < getEdge(kNum, lNum).getWeight()) {
                            removeEdge(kNum, lNum);
                            addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                        }
                    }
                }
            }
            order.add(min);
            done.set(min, true);
        }
        // System.out.println(edgeNum);

        return order;
    }

    private ArrayList<Integer> dpc(ArrayList<Boolean> irrelevant) {
        ArrayList<Integer> order = new ArrayList<Integer>();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            // done.add( (getDregree(j) <= 1) ? true:false);
            done.add(false);
        }

        for (int i = 0; i < size; i++) {
            if ( i % 1000 == 0) System.out.println("DPC iteration : " + i);
            int min = -1;
            for (int j = 0; j < size; j++ ) {
                if ( !done.get(j) && !irrelevant.get(j) && ( min == -1 || (getDregree(min) > getDregree(j)) )) min = j;
            }
            if (min == -1) break;

            LinkedList edge = adjList.get(min).getHead();
            ArrayList<LinkedList> edgeList = new ArrayList<LinkedList>();

            while(!edge.isEnd()) {
                if (!done.get(edge.getVertex())) edgeList.add(edge);
                edge = edge.getNext();
            }

            for (int k = 0; k < edgeList.size() ; k++ ) {
                for(int l = 0; l < edgeList.size(); l++) {
                    int lNum = edgeList.get(l).getVertex();
                    int kNum = edgeList.get(k).getVertex();
                    if(l < k && !edgeExist(kNum, lNum) ) {
                        addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                    } else if ( l < k ) {
                        if ( edgeList.get(k).getWeight() + edgeList.get(l).getWeight() < getEdge(kNum, lNum).getWeight()) {
                            removeEdge(kNum, lNum);
                            addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                        }
                    }
                }
            }
            order.add(min);
            done.set(min, true);
        }
        // System.out.println(edgeNum);
        return order;
    }

    public Double[][] snowBall() {
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        // g.removeDeg2(done);

        ArrayList<Integer> Rorder = g.dpc();
        ArrayList<Integer> order = new ArrayList<Integer>();
        ArrayList<Integer> orderRef = new ArrayList<Integer>();
        Double[][] distance = new Double[size][size];

        // Double[] index = new Double[];


        for (int i = 0; i < size ; i++ ) {
            orderRef.add(-1);
            for (int j = 0 ; j < size ; j++ ) {
                distance[i][j] = Double.MAX_VALUE;
            }
        }

        // ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            distance[j][j] = 0.0;

            // done.add( (getDregree(j) <= 1) ? true:false);
            // done.add(false);
            order.add(Rorder.get(size - 1 - j));
            orderRef.set(Rorder.get(size - 1 - j), j);
        }

        for (int k  = 0; k < size; k++ ) {
            if ( k % 1000 == 0) System.out.println("SB iteration : " + k);

            LinkedList edge = g.getEdges(order.get(k)).getHead();
            ArrayList<LinkedList> edgeList = new ArrayList<LinkedList>();

            while(!edge.isEnd()) {
                // if (order.indexOf(edge.getVertex()) < k) edgeList.add(edge);
                if (orderRef.get(edge.getVertex()) < k) edgeList.add(edge);
                edge = edge.getNext();
            }

            for (int j = 0; j < edgeList.size() ; j++ ) {
                for (int i = 0; i < k ; i++) {
                    double newWeight = distance[order.get(i)][edgeList.get(j).getVertex()] + edgeList.get(j).getWeight();
                    if ( distance[order.get(i)][order.get(k)] > newWeight) {
                        distance[order.get(i)][order.get(k)] = newWeight;
                        distance[order.get(k)][order.get(i)] = newWeight;
                    }
                }
            }
        }

        return distance;
    }

    public Double[][] snowBallRD2(ArrayList<Integer> order) {
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        ArrayList<Integer> orderRef = new ArrayList<Integer>();

        for (int j = 0; j < size; j++ ) {
            orderRef.add(-1);
            // done.add( (getDregree(j) <= 1) ? true:false);
            done.add(false);
        }
        g.removeDeg2(done);

        ArrayList<Integer> Rorder = g.dpc(done);
        Double[][] distance = new Double[Rorder.size()][Rorder.size()];
        for (int i = 0; i < Rorder.size() ; i++ ) {
            for (int j = 0 ; j < Rorder.size() ; j++ ) {
                distance[i][j] = Double.MAX_VALUE;
            }
        }

        // ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < Rorder.size(); j++ ) {

            distance[j][j] = 0.0;

            // done.add( (getDregree(j) <= 1) ? true:false);
            // done.add(false);
            order.add(Rorder.get( Rorder.size() - 1 - j));
            orderRef.set(Rorder.get(Rorder.size() - 1 - j), j);
        }

        System.out.println(order.size());

        for (int k  = 0; k < order.size(); k++ ) {
            if ( k % 1000 == 0) System.out.println("SB iteration : " + k);

            LinkedList edge = g.getEdges(order.get(k)).getHead();
            ArrayList<LinkedList> edgeList = new ArrayList<LinkedList>();

            while(!edge.isEnd()) {
                ///SHITTTTTTTT MORE O(n)
                if (orderRef.get(edge.getVertex()) < k) edgeList.add(edge);
                edge = edge.getNext();
            }

            for (int j = 0; j < edgeList.size() ; j++ ) {
                for (int i = 0; i < k ; i++) {
                    double newWeight = distance[i][order.indexOf(edgeList.get(j).getVertex())] + edgeList.get(j).getWeight();
                    if ( distance[i][k] > newWeight) {
                        distance[i][k] = newWeight;
                        distance[k][i] = newWeight;
                    }
                }
            }
        }

        return distance;
    }

    public int BFS(int start) {
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        ArrayList<Integer> nodes = new ArrayList<Integer>();
        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        nodes.add(start);
        int count = 1;
        done.set(start, true);

        while(!nodes.isEmpty()) {
            LinkedList edge = adjList.get(nodes.get(0)).getHead();
            while(!edge.isEnd()) {
                if ( !done.get(edge.getVertex()) ) {
                    nodes.add(edge.getVertex());
                    count++;
                    done.set(edge.getVertex(), true);
                }
                edge = edge.getNext();
            }
        nodes.remove(0);
        }

        return count;
    }

    public boolean sameSubGraph(int v, int u) {
        ArrayList<Boolean> done = new ArrayList<Boolean>();
        ArrayList<Integer> nodes = new ArrayList<Integer>();
        ArrayList<Integer> n = new ArrayList<Integer>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }
        nodes.add(v);
        n.add(v);
        done.set(v, true);

        while(!nodes.isEmpty()) {
            LinkedList edge = adjList.get(nodes.get(0)).getHead();
            while(!edge.isEnd()) {
                if ( !done.get(edge.getVertex()) ) {
                    nodes.add(edge.getVertex());
                    n.add(edge.getVertex());
                    done.set(edge.getVertex(), true);
                }
                edge = edge.getNext();
            }
        nodes.remove(0);
        }

        return (n.indexOf(u) != -1);
    }

    public Graph NDPCalgo() {
        Graph g = copy();
        g.dpc();
        return g;
    }

    private void Ndpc() {
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int i = adjList.size() - 1; i > -1; i--) {
            if ( i % 1000 == 0) System.out.println("DPC iteration : " + i);

            LinkedList edge = adjList.get(i).getHead();
            ArrayList<LinkedList> edgeList = new ArrayList<LinkedList>();

            while(!edge.isEnd()) {
                if (edge.getVertex() < i) edgeList.add(edge);
                edge = edge.getNext();
            }


            for (int k = 0; k < edgeList.size() ; k++ ) {
                for(int l = 0; l < edgeList.size(); l++) {
                    int lNum = edgeList.get(l).getVertex();
                    int kNum = edgeList.get(k).getVertex();
                    if(l < k && !edgeExist(kNum, lNum) ) {
                        addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                    } else if ( l < k ) {
                        if ( edgeList.get(k).getWeight() + edgeList.get(l).getWeight() < getEdge(kNum, lNum).getWeight()) {
                            removeEdge(kNum, lNum);
                            addEdge(kNum, lNum, edgeList.get(k).getWeight() + edgeList.get(l).getWeight());
                        }
                    }
                }
            }
        }
        System.out.println("DPC END");
    }

    public Double[][] NsnowBall() {
        Graph g = copy();
        g.Ndpc();
        Double[][] distance = new Double[size][size];
        for (int i = 0; i < size ; i++ ) {
            for (int j = 0 ; j < size ; j++ ) {
                distance[i][j] = Double.MAX_VALUE;
            }
        }

        // ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            distance[j][j] = 0.0;
        }

        for (int k  = 0; k < size; k++ ) {
            LinkedList edge = g.getEdges(k).getHead();

            while(!edge.isEnd()) {
                ///SHITTTTTTTT MORE O(n)
                if (edge.getVertex() < k) {
                    for (int i = 0; i < k ; i++) {
                        double newWeight = distance[i][edge.getVertex()] + edge.getWeight();
                        if ( distance[i][k] > newWeight) {
                            distance[i][k] = newWeight;
                            distance[k][i] = newWeight;
                        }
                    }
                }
                edge = edge.getNext();

            }

        }

        return distance;
    }

    public int treeWidthLMinDegree(double bufferSize) {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Boolean> done = new ArrayList<Boolean>();

        for (int j = 0; j < size; j++ ) {
            done.add(false);
        }

        for (int i = 0; i < size; i++ ) {
            if(g.getDregree(i) == 0) {
                done.set(i, true);
            }
        }

        for (int i = 0; i < Math.ceil(size / bufferSize); i++ ) {
            BinHeap buffer = new BinHeap<Integer>();
            if (i % (10) == 0 )System.out.println("Iteration: " + i + " TW: " + treewidthBound);
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                // if ( done.indexOf(j) == -1 && ( min == -1 || (g.getDregree(min) > g.getDregree(j)) )) min = j;
                if ( !done.get(j) ) buffer.insert(new KVP<Integer>(g.getDregree(j), j));
            }

            if(buffer.size() == 0) break;

            int couter = 0;
            while(couter++ < bufferSize && buffer.size() != 0) {
                min = (int) buffer.getMin().getValue();
                if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
                g.removeVertex(min);
                done.set(min, true);
            }

        }
        return treewidthBound;
    }

    public void removeVertex(int v) {

        ArrayList<Integer> nodes = new ArrayList<Integer>();
        LinkedList edge = adjList.get(v).getHead();

        while (!edge.isEnd()) {
            nodes.add(edge.getVertex());
            edge = edge.getNext();
        }

        for (int i = 0; i < nodes.size(); i++) {
            removeEdge( nodes.get(i), v);
        }

    }


}
