class FibHeapElem {

     private FibHeapElem next;
     private FibHeapElem previous;
     private FibHeapElem parent;
     private FibHeapElem child;
     private int subTreeSize;
     private boolean mark;

     //String is node in a graph
     private double priority;
     private int identifer;

     public FibHeapElem(double num, Integer identifer) {
          this.priority = num;
          this.identifer = identifer;
          this.next = null;
          this.previous = null;
          this.parent = null;
          this.child = null;
          this.subTreeSize = 1;
          this.mark = false;
     }


     public FibHeapElem(double num, Integer identifer, FibHeapElem next) {
          this.priority = num;
          this.identifer = identifer;
          this.next = next;
          this.previous = null;
          this.parent = null;
          this.child = null;
          this.subTreeSize = 1;
          this.mark = false;

     }

     public FibHeapElem(double num, Integer identifer, FibHeapElem next, FibHeapElem previous) {
          this.priority = num;
          this.identifer = identifer;
          this.next = next;
          this.previous = previous;
          this.parent = null;
          this.child = null;
          this.subTreeSize = 1;
          this.mark = false;
     }

     public FibHeapElem() {
          this.priority = -1;
          this.identifer = -1;
          this.next = null;
          this.previous = null;
          this.parent = null;
          this.child = null;
          this.subTreeSize = 1;
          this.mark = false;
     }

     public boolean hasNext() {
          return (next != null);
     }

     public boolean hasPrevious() {
          return (previous != null);
     }

     public boolean hasParent() {
          return (parent != null);
     }

     public boolean hasChild() {
          return (child != null);
     }


     public boolean isMarked() {
          return mark;
     }

     public void setNext(FibHeapElem next) {
          // if(!(next == null)) {
               // System.out.println("setting " + next.getIdentifer() +  " after " + identifer);
          // } else {
               // System.out.println("Removing next from " + identifer);
          // }
          this.next = next;
     }

     public void setPrevious(FibHeapElem previous) {
          if(!(previous == null)) {
               // System.out.println("setting " + previous.getPriority() +  " before " + priority);
          }
          this.previous = previous;
     }

     public void setParent(FibHeapElem parent) {
          this.parent = parent;
     }

     public void setChild(FibHeapElem newChild) {
          if (hasChild()) {
               child.setPrevious(newChild);
               newChild.setNext(child);
          }
          else {
               newChild.setNext(null);
          }
          newChild.setPrevious(null);
          child = newChild;
          subTreeSize += newChild.getSize();
     }

     public void removeChild() {
          child = null;
     }

     public void markNode() {
          mark = true;
     }

     public void unMarkNode() {
          mark = false;
     }

     public void resetTreeSize() {
          subTreeSize = 1;
     }

     public void decreseTreeSize(int size) {
          subTreeSize -= size;
          if (hasParent()) {
               parent.decreseTreeSize(size);
          }
     }


     public void increseTreeSize(int size) {
          subTreeSize += size;
          if (hasParent()) {
               parent.increseTreeSize(size);
          }
     }

     public FibHeapElem getNext() {
          return next;
     }

     public FibHeapElem getPrevious() {
          return previous;
     }

     public FibHeapElem getParent() {
          return parent;
     }

     public FibHeapElem getChild() {
          return child;
     }

     public int getSize() {
          return subTreeSize;
     }


     public double getPriority() {
          return priority;
     }

     public void setPrio(double newPrio) {
          priority = newPrio;
     }

     public boolean isEqual(FibHeapElem elem) {
          return (!(elem == null) && priority == elem.getPriority() && identifer == elem.getIdentifer());
     }

     public int getIdentifer() {
          return identifer;
     }

     public void cutChild() {
          child = child.getNext();
          child.setPrevious(null);
     }



}
