import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;


class OldParseOSM {


    public static void main(String[] args) throws IOException{
        String fileName = "Oxford";
        File file = new File(".\\Oxford");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        String node = "(<node\\s)(id=\")(\\d*)(.*)(lat=\")((-?+)\\d*)(.)(\\d*)(.*)(lon=\")((-?+)\\d*)(.)(\\d*)(.*)(>)";
        String way = "(<way\\s)(id=\")(\\d*)(.*)(>)";
        String wayNode = "(\\s*)(<nd\\s)(ref=\")(\\d*)(\")(.*)(>)";
        String tagRoad = "(<tag)(.*)(highway)(.*)(motorway|trunk|primary|secondary|tertiary|unclassified|residential|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link|living_street)(.*)";
        //String tagRoad = "(<tag)(.*)(highway)(.*)(motorway|trunk|primary|secondary|tertiary|unclassified|residential|service|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link|living_street)(.*)";
        String oneway = "(<tag)(.*)(oneway)(.*)(yes)";



        Pattern nodeP = Pattern.compile(node);
        Pattern wayP = Pattern.compile(way);
        Pattern wayNodeP = Pattern.compile(wayNode);
        Pattern tagRoadP = Pattern.compile(tagRoad);
        Pattern onewayP = Pattern.compile(oneway);


        //All nodes in the file
        OSMNodeMap allNodes = new OSMNodeMap();

        //All nodes in way
        ArrayList<Long> usedNodes = new ArrayList<Long>();

        //Way lists
        ArrayList<Long[]> edges = new ArrayList<Long[]>();


        while ((st = br.readLine()) != null) {

            String line = st;
            Matcher nodeM = nodeP.matcher(line);
            Matcher wayM = wayP.matcher(line);


            if (nodeM.find( )) {

                Long num = Long.valueOf(nodeM.group(3));
                double lat = Double.valueOf(nodeM.group(6) +  nodeM.group(8)+  nodeM.group(9));
                double lon = Double.valueOf(nodeM.group(12) + nodeM.group(14) + nodeM.group(15));
                allNodes.addNode(num, lat, lon);

             } else if (wayM.find()) {

                String wayElement = "";
                ArrayList<Long> nodes = new ArrayList<Long>();
                boolean onewayroad = false;
                String roadtype = "";
                while(!(wayElement.contains("</way>"))) {
                    st = br.readLine();
                    wayElement = st;

                    Matcher wayNodeM = wayNodeP.matcher(wayElement);
                    Matcher tagRoadM = tagRoadP.matcher(wayElement);
                    Matcher onewayM = onewayP.matcher(wayElement);
                    if (wayNodeM.find()) {
                       nodes.add(Long.valueOf(wayNodeM.group(4)));
                    } else if (tagRoadM.find()) {
                        roadtype = tagRoadM.group(5);
                    } else if (onewayM.find()) {
                        onewayroad = true;
                    }
                }

                if (!roadtype.equals("") ) {

                    OSMNode n;
                    int i;
                    for (i = 0; i < nodes.size() -1; i++ ) {

                        n = allNodes.getNode(nodes.get(i));
                        if (usedNodes.indexOf(n.getNum()) ==  -1)
                        {
                            usedNodes.add(n.getNum());
                        }
                        Long[] e = { nodes.get(i), nodes.get(i+1) };

                        edges.add( e );
                    }
                    n = allNodes.getNode(nodes.get(i));
                    if (usedNodes.indexOf(n.getNum()) ==  -1)
                    {
                        usedNodes.add(n.getNum());
                    }

                }

            }
             // else {
             //      System.out.println("NO MATCH");
             // }
        }
        PrintWriter writer = new PrintWriter(fileName + "edges.txt", "UTF-8");


        OSMNodeMap nodesToAdd = new OSMNodeMap();
        int i;
        Graph g = new Graph(0);
        for (i = 0; i < usedNodes.size() ; i++ ) {
            // System.out.println("Node " + usedNodes.get(i) +  " = " + i);
            OSMNode n = allNodes.getNode(usedNodes.get(i));
            // System.out.println("----------");
            // System.out.println(n.getNum() + " lat " + n.getLat() + " lon " + n.getLon());
            nodesToAdd.addNode(n);
            n = nodesToAdd.getNodeRef(i);
            // System.out.println(n.getNum() + " lat " + n.getLat() + " lon " + n.getLon());
            g.addVertex(n.getLat(), n.getLon());
            // System.out.println(g.getVertexLoc(i)[0]);
        }


        for ( i = 0; i < edges.size(); i++) {
            Long[] e = edges.get(i);
            // System.out.println("Node: " + e[0] + " (" + nodesToAdd.getRef(e[0]) + ")" + ",  " + e[1] + " (" + nodesToAdd.getRef(e[1]) + ")");
            writer.println("Node: " + e[0] + " (" + nodesToAdd.getRef(e[0]) + ")" + ",  " + e[1] + " (" + nodesToAdd.getRef(e[1]) + ")");
            g.addEdge(nodesToAdd.getRef(e[1]), nodesToAdd.getRef(e[0]));
        }
        // <node id="395987" visible="true" version="6" changeset="7454179" timestamp="2011-03-04T12:54:54Z" user="Richard Mann" uid="74570" lat="51.7435854" lon="-1.3039737"/>
        // <node id="395981" visible="true" version="7" changeset="18996920" timestamp="2013-11-19T16:25:03Z" user="GordonFS" uid="782788" lat="51.7498338" lon="-1.3024112"/>
        // 395987 top hrr
        // 395981
        // g = g.removeDeg2();
        System.out.println("dsods");
        g.toFile();
        writer.close();

        for (int j = 0; j < g.getSize() - 1 ; j++ ) {
            if(g.getDregree(j) == 0) {
                System.out.println(j + " " + usedNodes.get(j));
                LinkedListHead vertexList = g.getEdges(j);
                LinkedList vertex = vertexList.getHead();
                while(!vertex.isEnd()) {
                    System.out.println(vertex.getVertex());
                    vertex = vertex.getNext();
                }

            }
        }

        // System.out.println(g.treeWidthUMinDegree());
        System.out.println(g.treeWidthUMinDegreeFillin());
        // System.out.println(g.treeWidthUMinFillin());

        // System.out.println("Node " + nodesToAdd.getNodeRef(163).getNum() +  " = " + 163);
        // System.out.println("Node " + nodesToAdd.getNodeRef(177).getNum() +  " = " + 177);

        // Double[] llpair = g.getVertexLoc(163);
        // System.out.println(nodesToAdd.getNodeRef().getLat() + " " + nodesToAdd.getNodeRef(i).getLon());


        // System.out.println(g.djkAlgo(49, 54));
        // ArrayList<Integer> path = g.djkAlgo(49, 54);
        // ArrayList<Double[]> ll = new ArrayList<Double[]>();
        // for (i = 0; i < path.size(); i++ ) {
        //     Double[] llpair = g.getVertexLoc(path.get(i));
        //     // System.out.println(nodesToAdd.getNodeRef(i).getLat() + " " + nodesToAdd.getNodeRef(i).getLon());
        //     System.out.println(llpair[0] + " " +llpair[1] );
        //     ll.add(llpair);
        // }

        // DistanceCalculator d = new DistanceCalculator();

        // System.out.println(d.distaceList(ll));

    }



}
