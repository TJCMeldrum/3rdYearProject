class LinkedListHead {

    private LinkedList head;
    private int length;
    private double lon;
    private double lat;


    public LinkedListHead() {
        head = new LinkedList();
        length = 0;
        lon = 0;
        lat = 0;
    }

    public LinkedListHead(LinkedList head) {
        this.head = new LinkedList();
        length = 0;
        add(head);
        lon = 0;
        lat = 0;
    }

    public LinkedListHead(int vertex, double weight) {
        this.head = new LinkedList();
        length = 0;
        add(vertex, weight);
        lon = 0;
        lat = 0;
    }

    public LinkedListHead(double lon, double lat) {
        head = new LinkedList();
        length = 0;
        this.lon = lon;
        this.lat = lat;
    }

    public void add(LinkedList next) {
        next.setNext(head);
        head = next;
        length++;
    }


    public void add(int vertex, double weight) {
        LinkedList next = new LinkedList(vertex, weight);
        next.setNext(head);
        head = next;
        length++;
    }

    public LinkedList getHead() {
        return head;
    }

    public int length() {
        return length;
    }

    public double lon() {
        return lon;
    }

    public double lat() {
        return lat;
    }

    public void setLength(int l) {
        length = l;
    }

    public void setHead(LinkedList n) {
        head = n;
    }
}
