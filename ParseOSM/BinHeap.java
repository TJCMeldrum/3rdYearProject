import java.util.ArrayList;

class BinHeap<V> {

    ArrayList<KVP<V>> heap;

    public BinHeap() {
        heap = new ArrayList<KVP<V>>();

    }

    public void insert(KVP kvp) {
        heap.add(kvp);
        swap(heap.size() - 1);
    }

    public void swap(int index) {
        int parent =(int) Math.floor((index-1) / 2);
        if (parent == index) return;

        if (heap.get(parent).getKey() > heap.get(index).getKey()) {
            KVP<V> temp = heap.get(parent);
            heap.set(parent, heap.get(index));
            heap.set(index, temp);
            swap(parent);
        }
    }

    public KVP<V> getMin() {
        try {
            KVP<V> min = heap.get(0);
            heap.set(0, heap.get(heap.size() - 1 ));
            heap.remove(heap.size() - 1);
            shiftDown(0);
            return min;
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Empty heap");
            return new KVP<V>(-1,null);
        }
    }

    public void shiftDown(int i) {
        int l = i*2 + 1;
        int r = l + 1;
        int min = i;

        if (l <= heap.size() - 1 && heap.get(l).getKey() <  heap.get(min).getKey()) {
            min = l;
        }
        if (r <= heap.size() - 1 && heap.get(r).getKey() <  heap.get(min).getKey()) {
            min = r;
        }

        if(min != i) {
            KVP<V> temp = heap.get(i);
            heap.set(i, heap.get(min));
            heap.set(min, temp);
            shiftDown(min);
        }
    }

    public void printHeap() {
        for (int i = 0; i < heap.size() - 1 ; i++ ) {
            System.out.println(i + ": " + heap.get(i).getValue());
        }
    }

    public int size() {
        return heap.size();
    }
}
