import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;


class ParseOSM {

    public ParseOSM() { }

    public Graph parseData(String fileName) throws IOException {
        File file = new File(".\\" + fileName);

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        String node = "(<node\\s)(id=\")(\\d*)(.*)(lat=\")((-?+)\\d*)(.)(\\d*)(.*)(lon=\")((-?+)\\d*)(.)(\\d*)(.*)(>)";
        String way = "(<way\\s)(id=\")(\\d*)(.*)(>)";
        String wayNode = "(\\s*)(<nd\\s)(ref=\")(\\d*)(\")(.*)(>)";
        String tagRoad = "(<tag)(.*)(highway)(.*)(motorway|trunk|primary|secondary|tertiary|unclassified|residential|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link|living_street)(.*)";
        //String tagRoad = "(<tag)(.*)(highway)(.*)(motorway|trunk|primary|secondary|tertiary|unclassified|residential|service|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link|living_street)(.*)";
        String oneway = "(<tag)(.*)(oneway)(.*)(yes)";



        Pattern nodeP = Pattern.compile(node);
        Pattern wayP = Pattern.compile(way);
        Pattern wayNodeP = Pattern.compile(wayNode);
        Pattern tagRoadP = Pattern.compile(tagRoad);
        Pattern onewayP = Pattern.compile(oneway);


        //All nodes in the file
        OSMNodeMap allNodes = new OSMNodeMap();

        //All nodes in way
        ArrayList<Long> usedNodes = new ArrayList<Long>();

        //Way lists
        ArrayList<Long[]> edges = new ArrayList<Long[]>();

        while ((st = br.readLine()) != null) {

            String line = st;
            Matcher nodeM = nodeP.matcher(line);
            Matcher wayM = wayP.matcher(line);


            if (nodeM.find( )) {

                Long num = Long.valueOf(nodeM.group(3));
                double lat = Double.valueOf(nodeM.group(6) +  nodeM.group(8)+  nodeM.group(9));
                double lon = Double.valueOf(nodeM.group(12) + nodeM.group(14) + nodeM.group(15));
                allNodes.addNode(num, lat, lon);

             } else if (wayM.find()) {

                String wayElement = "";
                ArrayList<Long> nodes = new ArrayList<Long>();
                boolean onewayroad = false;
                String roadtype = "";
                while(!(wayElement.contains("</way>"))) {
                    st = br.readLine();
                    wayElement = st;

                    Matcher wayNodeM = wayNodeP.matcher(wayElement);
                    Matcher tagRoadM = tagRoadP.matcher(wayElement);
                    Matcher onewayM = onewayP.matcher(wayElement);
                    if (wayNodeM.find()) {
                       nodes.add(Long.valueOf(wayNodeM.group(4)));
                    } else if (tagRoadM.find()) {
                        roadtype = tagRoadM.group(5);
                    } else if (onewayM.find()) {
                        onewayroad = true;
                    }
                }

                if (!roadtype.equals("") ) {

                    OSMNode n;
                    int i;
                    for (i = 0; i < nodes.size() -1; i++ ) {

                        n = allNodes.getNode(nodes.get(i));
                        if (usedNodes.indexOf(n.getNum()) ==  -1)
                        {
                            usedNodes.add(n.getNum());
                        }
                        Long[] e = { nodes.get(i), nodes.get(i+1) };

                        edges.add( e );
                    }
                    n = allNodes.getNode(nodes.get(i));
                    if (usedNodes.indexOf(n.getNum()) ==  -1)
                    {
                        usedNodes.add(n.getNum());
                    }

                }

            }
        }
        PrintWriter writer = new PrintWriter(fileName + "edges.txt", "UTF-8");

        OSMNodeMap nodesToAdd = new OSMNodeMap();
        int i;
        Graph g = new Graph(0);
        for (i = 0; i < usedNodes.size() ; i++ ) {
            OSMNode n = allNodes.getNode(usedNodes.get(i));
            nodesToAdd.addNode(n);
            n = nodesToAdd.getNodeRef(i);
            g.addVertex(n.getLat(), n.getLon());
            // g.addVertex(n.getLat(), n.getLon(), (int) n.getNum());
        }


        for ( i = 0; i < edges.size(); i++) {
            Long[] e = edges.get(i);
            writer.println("Node: " + e[0] + " (" + nodesToAdd.getRef(e[0]) + ")" + ",  " + e[1] + " (" + nodesToAdd.getRef(e[1]) + ")");
            g.addEdge(nodesToAdd.getRef(e[1]), nodesToAdd.getRef(e[0]));
        }
        System.out.println("Finished Parsing");
        g.toFile();
        writer.close();

        return g;

    }

    public Graph parseGraph(String fileName) throws IOException {
        File file = new File(".\\" + fileName);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        // String node = "(N:)(\\d*)(.Lat:)(((-?)\\d*)(.)(\\d*))(.Lon:)(((-?)\\d*)(.)(\\d*))(.OSM:)(\\d*)";
        String node = "(N:)(\\d*)(.Lat:)(((-?)\\d*)(.)(\\d*))(.Lon:)(((-?)\\d*)(.)(\\d*))";
        Pattern nodeP = Pattern.compile(node);
        String edge = "(u:)(\\d*)(.)(v:)(\\d*)(.*)";
        Pattern edgeP = Pattern.compile(edge);

        Graph g = new Graph(0);

        while ((st = br.readLine()) != null) {

            String line = st;
            Matcher nodeM = nodeP.matcher(line);
            Matcher edgeM = edgeP.matcher(line);

            if ( nodeM.find() ) {
                // System.out.println("node");

                // Long num = Long.valueOf(nodeM.group(2));

                double lat = Double.valueOf(nodeM.group(4));
                double lon = Double.valueOf(nodeM.group(10));
                // int OSM = Integer.valueOf(16);

                g.addVertex(lat, lon);
                // g.addVertex(lat, lon, OSM);

            } else if (edgeM.find()) {
                // System.out.println("egde");
                int u = Integer.valueOf(edgeM.group(2));
                int v = Integer.valueOf(edgeM.group(5));
                g.addEdge(u, v);
            }


        }
        System.out.println("Parsing Finished");

        return g;

    }

    public Graph parseLTWGraph(String fileName) throws IOException {

        Graph g = new Graph(0);

        File file = new File(".\\" + fileName);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String nodeNum = "(Nodes\\s)(\\d*)";
        Pattern nodeNumP = Pattern.compile(nodeNum);

        String edge = "(E\\s)(\\d*)(\\s)(\\d*)(\\s)(\\d*)";
        Pattern edgeP = Pattern.compile(edge);

        String twdecom = "(b)((\\s\\d*))*";
        Pattern twdecomP = Pattern.compile(twdecom);

        String st;

        int c1 = 0;


        if ( (st = br.readLine()) == null ) {
            System.out.println("Failed");
            return g;
        }
        if ( (st = br.readLine()) != null ) {
            String line = st;
            Matcher nodeNumM = nodeNumP.matcher(line);

            if ( nodeNumM.find() ) {
                c1++;
                int num = Integer.valueOf(nodeNumM.group(2));
                for (int i = 0; i < num; i++ ) {
                    g.addVertex();
                }
            } else {
                System.out.println("Failed");
                return g;
            }
        } else {
            System.out.println("Failed");
            return g;
        }

        int c2 = 0;
        int c3 = 0;

        int tw = 0;

        while ((st = br.readLine()) != null) {
            String line = st;
            Matcher edgeM = edgeP.matcher(line);
            Matcher twdecomM = twdecomP.matcher(line);

            if (edgeM.find()) {
                c2++;
                int u = Integer.valueOf(edgeM.group(2));
                int v = Integer.valueOf(edgeM.group(4));
                int weight = Integer.valueOf(edgeM.group(6));

                g.addEdge(u - 1, v - 1, weight);

            } else if (twdecomM.find()) {
                String bucket = twdecomM.group();
                // System.out.println(twdecomM.group(0));
                // System.out.println(twdecomM.group(1));
                // System.out.println(twdecomM.group(2));
                // System.out.println(twdecomM.group(3));

                int bSize = 0;

                for (int i = 0; i < bucket.length() ; i++ ) {
                    if(bucket.charAt(i) == ' ') {
                        bSize++;
                    }
                }

                if (bSize > tw) {
                    tw = bSize;
                }
            }
        }

        // System.out.println(c1);
        // System.out.println(c2);
        // System.out.println(c3);
        System.out.println(tw);
        return g;
    }

    public int GraphTW(String fileName) throws IOException {


        File file = new File(".\\" + fileName);
        BufferedReader br = new BufferedReader(new FileReader(file));


        String twdecom = "(b)((\\s\\d*))*";
        Pattern twdecomP = Pattern.compile(twdecom);

        String st;

        int tw = 0;

        while ((st = br.readLine()) != null) {
            String line = st;
            Matcher twdecomM = twdecomP.matcher(line);

            if (twdecomM.find()) {
                String bucket = twdecomM.group();

                int bSize = 0;

                for (int i = 0; i < bucket.length() ; i++ ) {
                    if(bucket.charAt(i) == ' ') {
                        bSize++;
                    }
                }

                if (bSize > tw) {
                    tw = bSize;
                }
            }
        }

        // System.out.println(c1);
        // System.out.println(c2);
        // System.out.println(c3);
        return tw - 1;
    }

}
