class KVP<V> {

    int key;
    V value;

    public KVP(int key, V value) {
        this.key = key;
        this.value = value;
    }

    public V getValue() {
        return value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setValue(V value) {
        this.value = value;
    }



}
