import java.io.*;
import java.util.Scanner;

class treewidth {


    public static void main(String[] args) throws IOException {

     Scanner sc = new Scanner(System.in);
     System.out.println("Enter Graphs you wish estimates TW");
     String fileName = sc.nextLine();
     ParseOSM parser = new ParseOSM();

     System.out.println("Enter Huststic You wish to use:\n 1) Degree\n 2) Fill In\n 3) Degree + Fill In\n");
     String hurstic = sc.nextLine();

     System.out.println("Enter batch size");
     String batch = sc.nextLine();
     int b = Integer.valueOf(batch);

    Graph g = parser.parseGraph(fileName);

    if(hurstic.equals("1")) {
        System.out.println("\n\n TW upper bound: \n" + g.treeWidthUMinDegree(b));
    } else if (hurstic.equals("2")) {
            System.out.println("\n\n TW upper bound: \n" + g.treeWidthUMinFillin(b));
    } else if (hurstic.equals("3")) {
            System.out.println("\n\n TW upper bound: \n" + g.treeWidthUMinDegreeFillin(b));
    } else {
        System.out.println("Sorry something went wrong");
    }







    }


}
