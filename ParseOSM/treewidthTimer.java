import java.io.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class treewidthTimer {
    public static void main(String[] args) throws IOException {
        PrintWriter writer = new PrintWriter("lowTW.txt", "UTF-8");
        writer.println("Graph,Nodes,Edges,Degree,Time,FillIn,Time,Degree+Fillin,Time");
        File dir = new File("./public");
        File[] allMaps = dir.listFiles();
        ParseOSM paser = new ParseOSM();

        // String name = "(\.\\\\Maps\\\\)(.*)";
        // String name = "(.*)(Maps.)(.*)";
        // System.out.println(name);
        // Pattern nameP = Pattern.compile(name);
        // System.out.println(nameP.toString());


        // String dsa = ".MapsBath";
        // System.out.println(dsa);
        // Matcher nameM1 = nameP.matcher(dsa);
        // System.out.println(nameM1.group(2));
        ArrayList<Long> timesDeg = new ArrayList<Long>();
        ArrayList<Long> timesFil = new ArrayList<Long>();
        ArrayList<Long> timesDegFil = new ArrayList<Long>();
        ArrayList<Integer> nodes = new ArrayList<Integer>();
        ArrayList<Integer> edges = new ArrayList<Integer>();
        ArrayList<Integer> twDeg = new ArrayList<Integer>();
        ArrayList<Integer> twFil = new ArrayList<Integer>();
        ArrayList<Integer> twDegFil = new ArrayList<Integer>();
        ArrayList<String> graphNames = new ArrayList<String>();


        for (int i = 0; i < allMaps.length; i++ ) {
            // if (allMaps[i].toString().contains("txt")) continue;
            Graph g = paser.parseLTWGraph(allMaps[i].toString());
            System.out.println(allMaps[i].toString());
            //Matcher nameM = nameP.matcher(allMaps[i].toString());
            String[] a = allMaps[i].toString().split("\\\\");
            System.out.println(a[2]);

            nodes.add(g.size());
            edges.add(g.getEdgeNum());

            long startTime = System.currentTimeMillis();
            twDeg.add(g.treeWidthUMinDegree());
            long endTime = System.currentTimeMillis();
            timesDeg.add(endTime - startTime);


            startTime = System.currentTimeMillis();
            twFil.add(g.treeWidthUMinDegreeFillin());
            endTime = System.currentTimeMillis();
            timesDegFil.add(endTime - startTime);

            startTime = System.currentTimeMillis();
            twDegFil.add(g.treeWidthUMinFillin());
            endTime = System.currentTimeMillis();
            timesFil.add(endTime - startTime);


            graphNames.add(allMaps[i].toString());
            System.out.println("Total execution time: " + (endTime - startTime));

        }
        for (int i = 0; i < allMaps.length; i++ ) {
            // System.out.println(graphNames.get(i));
            // System.out.println(times.get(i));
            // writer.println("Graph,Nodes,Edges,Degree,Time,FillIn,Time,Degree+Fillin,Time");

            writer.println( graphNames.get(i) + "," + nodes.get(i) + "," + edges.get(i) + "," + twDeg.get(i) + "," + timesDeg.get(i) + "," + twFil.get(i) + "," + timesFil.get(i) + "," + twDegFil.get(i) + "," + timesDegFil.get(i));
        }
        writer.close();
    }
}
