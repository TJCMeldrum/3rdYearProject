import java.io.*;
import java.util.Scanner;

class FileToGraph {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter OSM you wish create a graphs of:");
        String fileName = sc.nextLine();
        ParseOSM parser = new ParseOSM();
        System.out.println("Enter name of graph:");
        String name = sc.nextLine();


        Graph g = parser.parseData(fileName);
        g.toFile(name);

    }


}
