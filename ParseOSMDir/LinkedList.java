class LinkedList {

     private int vertex;
     private double weight;
     private LinkedList next;
     private boolean hasNext;
     private boolean end;
     private boolean direction; // True: To vertex, False From Vertex

     public LinkedList(int vertex, double weight) {
          this.vertex = vertex;
          this.weight = weight;
          hasNext = false;
          end = false;
          direction = true;
     }

     public LinkedList(int vertex, double weight, boolean direction) {
          this.vertex = vertex;
          this.weight = weight;
          hasNext = false;
          end = false;
          this.direction = direction;
     }


     public LinkedList() {
          this.vertex = -1;
          this.weight = -1;
          hasNext = false;
          end = true;
     }

     public void setNext(LinkedList newNext) {
          this.next = newNext;
     }

     public boolean hasNext() {
          return hasNext;
     }

     public int getVertex() {
          return vertex;
     }

     public double getWeight() {
          return weight;
     }

     public LinkedList getNext() {
          return next;
     }

     public boolean isEnd() {
          return end;
     }

     public boolean getDir() {
         return direction;
     }

}
