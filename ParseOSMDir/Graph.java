import java.util.ArrayList;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class Graph {

    private int size;
    private ArrayList<LinkedListHead> adjList;


    public Graph(int size) {
        this.size = size;
        adjList = new ArrayList<LinkedListHead>(size);
    }

    public Double[] getVertexLoc(int v) {
        LinkedListHead vNode = adjList.get(v);
        Double[] ll = { vNode.lat(), vNode.lon() };
        return ll;
    }

    public void addEdge(int v, int u, int weight) {
        try{

            adjList.get(v).add(u, weight, true);
            adjList.get(u).add(v, weight, false);
        } catch (IndexOutOfBoundsException e ) {
            System.out.println("Invalid Edge");
        }
    }

    public void addEdge(int v, int u) {
        try{
            double R = 6371e3;
            double vlon = Math.toRadians(adjList.get(v).lon());
            double vlat = Math.toRadians(adjList.get(v).lat());
            double ulon =Math.toRadians(adjList.get(u).lon());
            double ulat = Math.toRadians(adjList.get(u).lat());
            double changeLat = (vlat - ulat);
            double changeLon = (vlon - ulon);

            double a = Math.sin(changeLat/2) * Math.sin(changeLat/2) + Math.cos(ulat) * Math.cos(vlat) * Math.sin(changeLon/2) * Math.sin(changeLon/2);

            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));


            double weight = R * c;
            // System.out.println("----------");
            // System.out.println(adjList.get(v).lat() + " " + adjList.get(v).lon());
            // System.out.println(adjList.get(u).lat() + " " + adjList.get(u).lon());
            // System.out.println(weight);

            adjList.get(v).add(u, weight, true);
            adjList.get(v).add(u, weight, false);

        } catch (IndexOutOfBoundsException e ) {
            // System.out.println("v" + v);
            // System.out.println("u" + u);
            System.out.println("Invalid Edge");
        }
    }

    public void addEdge(int v, int u, double weight) {
        adjList.get(v).add(u, weight, true);
        adjList.get(u).add(v, weight, false);
    }


    public int addVertex (double lat, double lon) {
        adjList.add(new LinkedListHead(lon, lat));
        size = adjList.size();
        return adjList.size() - 1;
    }

    public LinkedListHead getEdges(int v) {
        return adjList.get(v);
    }

    public int getDregree(int v) {
        return adjList.get(v).length();
    }

    public LinkedList getEdge (int v, int u) {
        LinkedList edge = getEdges(v).getHead();
        while (!edge.isEnd()) {
            if (edge.getVertex() == u && edge.getDir()) {
                return edge;
            }
            edge = edge.getNext();
        }
        return edge;
    }

    public int getSize() {
        return size;
    }


    public ArrayList<Integer> djkAlgo(int start, int end) {
        double[] distace = new double[size];
        int[] previous = new int[size];
        PrioQueue queue = new PrioQueue(size);
        for (int i = 0; i < size ; i++ ) {
            distace[i] = Integer.MAX_VALUE;
            queue.insert(i, distace[i]);
            previous[i] = -1;
        }

        distace[start] = 0;
        queue.decreseKey(start, 0);
        previous[start] = start;

        int currentVertex = queue.extractMin();

        while (currentVertex != end) {
            // System.out.println(currentVertex);
            if (previous[currentVertex] == -1) {
                break;
            }
            LinkedList edge = adjList.get(currentVertex).getHead();
            while (!(edge.isEnd())) {
                relaxEdge(currentVertex, edge.getVertex(), edge.getWeight(), distace, previous, queue);
                edge = edge.getNext();
            }
            currentVertex = queue.extractMin();
        }

        Integer pathNode = end;
        ArrayList<Integer> path = new ArrayList<Integer>();
        // System.out.println(end);
        // String path = "";
        do {
            // path = path + pathNode.toString() + ", ";
            path.add(pathNode);
            pathNode = previous[pathNode];
        } while (pathNode != start);
        // path = path + pathNode.toString();
        path.add(pathNode);
        System.out.println(distace[end]);

        return path;
    }

    private void relaxEdge(int v, int u, double weight, double[] distace, int[] previous, PrioQueue q) {
        if (distace[u] >= distace[v] + weight) {
            distace[u] = distace[v] + weight;
              // System.out.println(distace[u]);
            previous[u] = v;
              // System.out.println("Q decrese");
            q.decreseKey(u, distace[u]);
              // System.out.println("Done");

        }
    }

    public Graph copy () {
        Graph g = new Graph(0);
        for (int i = 0;  i < adjList.size() ; i++ ) {
            LinkedListHead node = adjList.get(i);
            g.addVertex(node.lat(),node.lon());
        }

        for(int i = 0;  i < adjList.size() ; i++ ) {
            LinkedListHead node = adjList.get(i);
            LinkedList edge = node.getHead();
            while(!edge.isEnd()) {
                if (edge.getDir()) {
                    g.addEdge(i,edge.getVertex(),edge.getWeight());
                }
                edge = edge.getNext();
            }
        }
        return g;

    }


    // public Graph removeDeg2() {
    //     ArrayList<Integer> toKeep = new ArrayList<Integer>();
    //     for (int i = 0; i < size ; i++ ) {
    //         if (adjList.get(i).length == 1) {
    //             if ()
    //             // System.out.println("rm calles");
    //         }
    //         else {
    //             toKeep.add(i);
    //         }
    //     }
    //     Graph newGraph = new Graph(0);
    //     for (int j = 0; j < toKeep.size(); j++ ) {
    //         int n = toKeep.get(j);
    //         newGraph.addVertex(adjList.get(j).lat(), adjList.get(j).lon());
    //     }
    //     for (int k = 0; k < toKeep.size(); k++ ) {
    //         // System.out.println(toKeep.get(k));
    //         LinkedList edge = adjList.get(toKeep.get(k)).getHead();
    //         // System.out.println(edge.isEnd());
    //         while(!edge.isEnd()) {
    //             // System.out.println("fsjdofsa");
    //             if (toKeep.get(k) > toKeep.indexOf(edge.getVertex())) {
    //                 newGraph.addEdge(k, toKeep.indexOf(edge.getVertex()), edge.getWeight());
    //             }
    //             edge = edge.getNext();
    //         }
    //     }
    //     return newGraph;
    //
    //
    //
    // }

    public void removeNode(int n) {
        // LinkedListHead node = adjList.get(n);
        // LinkedList edgev = node.getHead();
        // LinkedList edgeu = node.getHead().getNext();
        // int vNum = edgev.getVertex();
        // int uNum = edgeu.getVertex();
        // // LinkedListHead vNode = adjList.get(vNum);
        // // LinkedListHead uNode = adjList.get(nNum);
        // removeEdge(vNum, n);
        // removeEdge(uNum, n);
        // addEdge(vNum, uNum, edgeu.getWeight() + edgev.getWeight());
    }

    // public void removeEdge(int v, int n) {
        // System.out.println("_------------------------------");
        // System.out.println("v" + v);
        // System.out.println("n" + n);
        // ArrayList<Integer> before = new ArrayList<Integer>();
        // ArrayList<Integer> after = new ArrayList<Integer>();
        // LinkedList test = adjList.get(v).getHead();
        // while (!test.isEnd()) {
        //     System.out.println("Before " + test.getVertex());
        //     before.add(test.getVertex());
        //     test = test.getNext();
        // }
        // LinkedList vEdgeSet = adjList.get(v).getHead();
        // int length = adjList.get(v).length() - 1;
        // if(vEdgeSet.getVertex() == n) {
        //     System.out.println(vEdgeSet.getVertex());
        //     adjList.get(v).setHead(vEdgeSet.getNext());
        // }
        // while (!vEdgeSet.isEnd()) {
        //     System.out.println(vEdgeSet.getNext().getVertex());
        //     if (vEdgeSet.getNext().getVertex() == n) {
        //         vEdgeSet.setNext(vEdgeSet.getNext().getNext());
        //         break;
        //     }
        //     vEdgeSet = vEdgeSet.getNext();
        // }
        // adjList.get(v).setLength(length);
        // test = adjList.get(v).getHead();
        // while (!test.isEnd()) {
        //     after.add(test.getVertex());
        //     test = test.getNext();
        // }
        //
        // if(before.size() - 1 != after.size()) {
        //     System.out.println(before.size());
        //     System.out.println(after.size());
        //     for (int i = 0; i < after.size(); i++ ) {
        //         System.out.println("after " + after.get(i));
        //     }
        //     System.out.println("SHITTTTTTTTTTTTTTTTTTT");
        //     System.exit(0);
        // }
    // }

    public void toFile() throws IOException {
        PrintWriter writer = new PrintWriter("Graph.txt", "UTF-8");
        for (int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            writer.println("N:" + i + " Lat:" + node.lat() + " Lon:" + node.lon());
        }
        for(int i = 0;  i < size ; i++ ) {
            LinkedListHead node = adjList.get(i);
            LinkedList edge = node.getHead();
            while(!edge.isEnd()) {
                if (edge.getDir()) {
                    writer.println("u:" + i + " v:" + edge.getVertex() + " w:" + edge.getWeight());
                }
                edge = edge.getNext();
            }
        }
        writer.close();

    }

    public int treeWidthUMinDegree() {
        int treewidthBound = -1;
        Graph g = copy();
        ArrayList<Integer> done = new ArrayList<Integer>();
        for (int i = 0; i < size; i++ ) {
            int min = -1;
            for (int j = 0; j < size ; j++ ) {
                if ((min == -1 || (g.getDregree(min) > g.getDregree(j))) && done.indexOf(j) == -1) min = j;
            }
            System.out.println("deg " + g.getDregree(min));
            System.out.println("node" + min);
            if (g.getDregree(min) > treewidthBound) treewidthBound = g.getDregree(min);
            g.eliminate(min);
            done.add(min);
        }
        return treewidthBound;
    }

    public void eliminate(int v) {
        ArrayList<Integer> fromV = new ArrayList<Integer>();
        ArrayList<Integer> toV = new ArrayList<Integer>();
        LinkedList edge = adjList.get(v).getHead();
        while (!edge.isEnd()) {
            if(edge.getDir()) fromV.add(edge.getVertex());
            else toV.add(edge.getVertex());
            edge = edge.getNext();
        }
        for (int i = 0; i < toV.size(); i++) {
            removeEdge(toV.get(i), v);
        }
        for (int i = 0; i < fromV.size(); i++) {
            removeEdge(fromV.get(i), v);
        }
        for (int i = 0; i < toV.size(); i++) {
            for (int j = 0; j < fromV.size(); j++) {
                if(getEdge(toV.get(i), fromV.get(j)).isEnd() && toV.get(i) != fromV.get(j))  {
                    System.out.println(toV.get(i) + " "  + fromV.get(j));
                    addEdge(toV.get(i), fromV.get(j), 1.0);
                }
            }
        }
    }


    private void removeEdge(int u, int v) {
        LinkedListHead uEdgeHead = adjList.get(u);
        LinkedList uEdge = uEdgeHead.getHead();

        if (uEdge.getVertex() == v) {
            uEdgeHead.setHead(uEdge.getNext());
        } else {
            while (!uEdge.isEnd()) {
                if (uEdge.getNext().getVertex() == v) {
                    uEdge.setNext(uEdge.getNext().getNext());
                    break;
                }
                uEdge = uEdge.getNext();
            }
        }
        uEdgeHead.setLength(uEdgeHead.length() - 1);
    }

}
