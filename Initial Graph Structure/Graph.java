class Graph {

     private int size;
     private LinkedListHead[] adjList;


     public Graph(int size) {
          this.size = size;
          adjList = new LinkedListHead[size];
          for (int i = 0; i < size; i++ ) {
               adjList[i] = new LinkedListHead();
          }
     }

     public void addEdge(int v, int u, int weight) {
          adjList[v].add(u, weight);
          adjList[u].add(v, weight);
     }

     public LinkedListHead getEdges(int v) {
          return adjList[v];
     }

     public int djkAlgo(int start, int end) {
          int[] distace = new int[size];
          int[] previous = new int[size];
          PrioQueue queue = new PrioQueue(size);
          for (int i = 0; i < size ; i++ ) {
               distace[i] = Integer.MAX_VALUE;
               queue.insert(i, distace[i]);
               previous[i] = -1;
          }

          distace[start] = 0;
          queue.decreseKey(start, 0);
          previous[start] = start;

          int currentVertex = queue.extractMin();

          while (currentVertex != end) {
               if (previous[currentVertex] == -1) {
                    break;
               }
               LinkedList edge = adjList[currentVertex].getHead();
               while (!(edge.isEnd())) {
                    relaxEdge(currentVertex, edge.getVertex(), edge.getWeight(), distace, previous, queue);
                    edge = edge.getNext();
               }
               currentVertex = queue.extractMin();
          }

          Integer pathNode = end;
          String path = "";
          do {
               path = path + pathNode.toString() + ", ";
               pathNode = previous[pathNode];
          } while (pathNode != start);
          path = path + pathNode.toString();
          System.out.println(path);

          return distace[end];
     }


     public int djkAlgo2Way(int start, int end) {
          int[] distaceS = new int[size];
          int[] distaceE = new int[size];
          int[] previousS = new int[size];
          int[] previousE = new int[size];
          PrioQueue queueS = new PrioQueue(size);
          PrioQueue queueE = new PrioQueue(size);
          boolean[] processedS = new boolean[size];
          boolean[] processedE = new boolean[size];



          for (int i = 0; i < size ; i++ ) {
               distaceS[i] = Integer.MAX_VALUE;
               distaceE[i] = Integer.MAX_VALUE;
               queueS.insert(i, distaceS[i]);
               queueE.insert(i, distaceE[i]);
               previousS[i] = -1;
               previousE[i] = -1;
               processedS[i] = false;
               processedE[i] = false;
          }

          distaceS[start] = 0;
          distaceE[end] = 0;
          queueS.decreseKey(start, 0);
          queueE.decreseKey(end, 0);
          previousS[start] = start;
          previousE[end] = end;

          int currentVertexS = queueS.extractMin();
          int currentVertexE = queueE.extractMin();

          while (!(processedE[currentVertexS]) && !(processedS[currentVertexE]) && currentVertexE != currentVertexS) {
               // System.out.println(currentVertexE + " end");
               // System.out.println(currentVertexS + " start");
               if (previousS[currentVertexS] == -1 || previousE[currentVertexE] == -1) {
                    break;
               }

               LinkedList edgeS = adjList[currentVertexS].getHead();
               LinkedList edgeE = adjList[currentVertexE].getHead();
               while (!(edgeS.isEnd())) {
                    relaxEdge(currentVertexS, edgeS.getVertex(), edgeS.getWeight(), distaceS, previousS, queueS);
                    edgeS = edgeS.getNext();
               }
               processedS[currentVertexS] = true;
               currentVertexS = queueS.extractMin();

               while (!(edgeE.isEnd())) {
                    relaxEdge(currentVertexE, edgeE.getVertex(), edgeE.getWeight(), distaceE, previousE, queueE);
                    edgeE = edgeE.getNext();
               }
               processedE[currentVertexE] = true;
               currentVertexE = queueE.extractMin();
          }
          // System.out.println(currentVertexE + " end");
          // System.out.println(currentVertexS + " start");

          // ArrayList<Integer> path = new ArrayList<Integer>();

          LinkedListHead path = new LinkedListHead();
          int distance = Integer.MAX_VALUE;
          int vertexS = -1;
          int vertexE = -1;
          String finalPath = "";

          if (currentVertexE == currentVertexS) {
               LinkedList edgeS = adjList[currentVertexS].getHead();
               LinkedList edgeE = adjList[currentVertexE].getHead();
               while (!(edgeS.isEnd())) {
                    relaxEdge(currentVertexS, edgeS.getVertex(), edgeS.getWeight(), distaceS, previousS, queueS);
                    edgeS = edgeS.getNext();
               }
               processedS[currentVertexS] = true;

               while (!(edgeE.isEnd())) {
                    relaxEdge(currentVertexE, edgeE.getVertex(), edgeE.getWeight(), distaceE, previousE, queueE);
                    edgeE = edgeE.getNext();
               }
               processedE[currentVertexE] = true;
          }

          for (int i = 0; i < size; i++ ) {
               if (!(processedS[i])) continue;
               LinkedList edge = adjList[i].getHead();
               while (!(edge.isEnd())) {
                    if(processedE[edge.getVertex()]) {
                         if ((distaceS[i] + distaceE[edge.getVertex()] + edge.getWeight()) < distance ) {
                                   distance = distaceS[i] + distaceE[edge.getVertex()] + edge.getWeight();
                                   vertexS = i;
                                   vertexE = edge.getVertex();
                         }
                    }
                    edge = edge.getNext();
               }
          }




          do {
               path.add(vertexS, -1);
               vertexS = previousS[vertexS];
          } while (vertexS != start);
          path.add(vertexS, -1);

          LinkedList node = path.getHead();

          while (!(node.isEnd())) {

               finalPath = finalPath + node.getVertex() + ", " ;
               node = node.getNext();
          }


          while (vertexE != end) {
               finalPath = finalPath + vertexE + ", ";
               vertexE = previousE[vertexE];
          }

          finalPath = finalPath + vertexE;







          // if (processed[currentVertexS]) {
          //      // System.out.println("FESDFASE");
          //      distance = distaceE[currentVertexS] + distaceS[currentVertexS];
          //      Integer vertex = currentVertexS;
          //
          //      do {
          //           path.add(vertex, -1);
          //           vertex = previousS[vertex];
          //      } while (vertex != start);
          //
          //      LinkedList node = path.getHead();
          //
          //      while (!(node.isEnd())) {
          //
          //           finalPath = finalPath + node.getVertex() + ", " ;
          //           node = node.getNext();
          //      }
          //
          //      vertex = currentVertexS;
          //
          //      while (vertex != end) {
          //           finalPath = finalPath + vertex.toString() + ", ";
          //           vertex = previousE[vertex];
          //      }
          //
          //      finalPath = finalPath + vertex.toString();
          //
          // } else if (processed[currentVertexE]) {
          //      distance = distaceE[currentVertexE] + distaceS[currentVertexE];
          //      System.out.println(distaceE[currentVertexE]);
          //      System.out.println(distaceS[currentVertexS]);
          //
          //      Integer vertex = currentVertexE;
          //
          //      do {
          //           path.add(vertex, -1);
          //           vertex = previousS[vertex];
          //      } while (vertex != start);
          //
          //      LinkedList node = path.getHead();
          //
          //      while (!(node.isEnd())) {
          //
          //           finalPath = finalPath + node.getVertex() + ", " ;
          //           node = node.getNext();
          //      }
          //
          //      vertex = currentVertexE;
          //
          //      while (vertex != end) {
          //           finalPath = finalPath + vertex.toString() + ", ";
          //           vertex = previousE[vertex];
          //      }
          //
          //      finalPath = finalPath + vertex.toString();
          // } else {
          //      distance = distaceE[currentVertexS] + distaceS[currentVertexS];
          //
          //      Integer vertex = currentVertexS;
          //
          //      do {
          //           path.add(vertex, -1);
          //           vertex = previousS[vertex];
          //      } while (vertex != start);
          //
          //      LinkedList node = path.getHead();
          //
          //      while (!(node.isEnd())) {
          //
          //           finalPath = finalPath + node.getVertex() + ", " ;
          //           node = node.getNext();
          //      }
          //
          //      vertex = currentVertexS;
          //
          //      while (vertex != end) {
          //           finalPath = finalPath + vertex.toString() + ", ";
          //           vertex = previousE[vertex];
          //      }
          //
          //      finalPath = finalPath + vertex.toString();
          // }

          System.out.println(finalPath);

          return distance;

          // Integer pathNode = end;
          // String path = "";
          // do {
          //      path = path + pathNode.toString() + ", ";
          //      pathNode = previous[pathNode];
          // } while (pathNode != start);
          // path = path + pathNode.toString();
          // System.out.println(path);
          //
          // return distace[end];
     }




     private void relaxEdge(int v, int u, int weight, int[] distace, int[] previous, PrioQueue q) {
          if (distace[u] >= distace[v] + weight) {
               distace[u] = distace[v] + weight;
               // System.out.println(distace[u]);
               previous[u] = v;
               // System.out.println("Q decrese");
               q.decreseKey(u, distace[u]);
               // System.out.println("Done");

          }
     }


}
