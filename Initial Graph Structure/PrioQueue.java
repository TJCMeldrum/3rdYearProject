import java.util.ArrayList;

class PrioQueue {

     FibHeap heap;
     ArrayList<FibHeapElem> elements;

     public PrioQueue (int size) {
          heap = new FibHeap();
          elements = new ArrayList<FibHeapElem>(size);
          for (int i = 0; i < size; i++ ) {
               elements.add(new FibHeapElem());
          }
     }

     public void insert( int identifer, int priority) {
          FibHeapElem elem = new FibHeapElem(priority, identifer);
          elements.set(identifer, elem);
          heap.insert(elem);
     }

     public int extractMin() {
          int identifer = heap.extractMin();
          elements.set(identifer, new FibHeapElem());
          // elements.trimToSoze();
          return identifer;
     }

     public void decreseKey(int identifer, int priority) {
          heap.decreseKey(elements.get(identifer), priority);
     }

     public boolean isEmpty() {
          return heap.isEmpty();
     }

     public void printQ() {
          heap.printTree(1);
          System.out.println("---------------");
     }


}
