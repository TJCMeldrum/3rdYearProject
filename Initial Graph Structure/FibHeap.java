class FibHeap {

     private int size;
     private FibHeapElem min;

     public FibHeap() {
          size = 0;
          min = null;
     }

     public FibHeap(FibHeapElem first) {
          size = 1;
          min = first;
     }

     public void insert(int priority, int identifer) {
          FibHeapElem elem = new FibHeapElem(priority, identifer);
          placeNode(elem);
     }

     public void insert(FibHeapElem elem) {
          clearNode(elem);
          placeNode(elem);

     }

     public void placeNode(FibHeapElem elem) {
          if (min == null) {
               min = elem;
               size++;
               return;
          }

          if (min.getPriority() > elem.getPriority()) {
               setBefore(min, elem);
               min = elem;
               size++;
               return;
          }

          else {
               setAfter(min, elem);
               size++;
               return;
          }
     }

     public void clearNode(FibHeapElem elem) {
          elem.setNext(null);
          elem.setPrevious(null);
          elem.removeChild();
          elem.setParent(null);
          elem.unMarkNode();
          elem.resetTreeSize();
     }

     public int extractMin() {
          if (min == null) {
               return -1;
          }
          int value = min.getIdentifer();
          // System.out.println(value);
          // if(min.hasChild()) {
          //      // System.out.println("Has child");
          //      FibHeapElem minsChild = min.getChild();
          //      removeElem(min);
          //      if (min.hasPrevious())
          //           setAfter(min.getPrevious(), minsChild);
          //      if (min.hasNext()) {
          //           // System.out.println("next");
          //           while (minsChild.hasNext()) {
          //                minsChild = minsChild.getNext();
          //           }
          //           setAfter(minsChild, min.getNext());
          //      }
          //      removeElem(min);
          // } else {
          //      removeElemList(min);
          // }




          // System.out.println("---------------");
          // printTree(min, 1);
          // System.out.println("---------------");

          FibHeapElem minNext = null;
          FibHeapElem minPre = null;
          FibHeapElem minChild = null;


          if (min.hasNext()) minNext = min.getNext();
          if (min.hasPrevious()) minPre = min.getPrevious();
          if (min.hasChild()) minChild = min.getChild();

          removeElemList(min);

          if (min.hasChild()) {
               if(min.hasNext()) {
                    while (minNext.hasNext()) {
                         minNext = minNext.getNext();
                    }
                    setAfter(minNext, minChild);
                    // minNext.setNext(min.getChild());
               }
               else if (min.hasPrevious()) {
                    setAfter(minPre, minChild);
                    // minPre.setNext(minChild);
                    // minChild.setPrevious(minPre);
               }
          }


          FibHeapElem[] treePointers = new FibHeapElem[size];

          FibHeapElem subTree;
          if(min.hasPrevious()) {
               // System.out.println(min.getIdentifer());
               subTree = min.getPrevious();
               while(subTree.hasPrevious()) {
                    subTree = subTree.getPrevious();
               }
          } else if (min.hasNext()) {
               subTree = min.getNext();
          } else if (min.hasChild()) {
               subTree = min.getChild();
          } else {
               size--;
               min = null;
               return value;
          }

          if (min.hasChild())
               removeParent(min.getChild());

          int maxTreeSize = 0;

          subTree = new FibHeapElem(-1, -1, subTree);
          // System.out.println(subTree.hasPrevious());

          // printTree(subTree, 1);
          while (subTree.hasNext()) {
               subTree = subTree.getNext();

               // System.out.println("Considering sub tree " + subTree.getIdentifer());
               if (subTree.hasPrevious())
                    // System.out.println("remvoing " + subTree.getPrevious().getPriority() + " + from previous");
               subTree.setPrevious(null);

               int treeSize = subTree.getSize();

               if (treePointers[treeSize] == null) {
                    // System.out.println("placing " + subTree.getPriority() + " in " + treeSize);
                    // System.out.println("Next tree is " + subTree.getNext().getPriority());
                    treePointers[treeSize] = subTree;
                    maxTreeSize = max(maxTreeSize, treeSize);
               }

               else {
                    if (subTree.getPriority() < treePointers[treeSize].getPriority()) {
                         // System.out.println("!Setting " + treePointers[treeSize].getIdentifer() + " as child of " + subTree.getIdentifer());
                         setChild(subTree, treePointers[treeSize]);
                         subTree = new FibHeapElem(-1, -1, subTree);
                         treePointers[treeSize] = null;
                    }
                    else {
                         // System.out.println("Setting " + subTree.getIdentifer() + " as child of " + treePointers[treeSize].getIdentifer());

                         if (subTree.hasNext()) {
                              // System.out.println("Is theis fnsjdiafbuqwophen;");
                              replaceNode(subTree, treePointers[treeSize]);

                         }
                         else {
                              // System.out.println("this sould trigger");
                              treePointers[treeSize].setNext(null);
                         }

                         setChild(treePointers[treeSize], subTree);
                         subTree = new FibHeapElem(-1, -1, treePointers[treeSize]);
                         treePointers[treeSize] = null;
                    }
               }
          }
          // System.out.println("fin");

          int lastTreeLocation = -1;
          for(int i = 0; i <= maxTreeSize; i++) {
               if (treePointers[i] != null) {
                    if (lastTreeLocation == -1) {
                         lastTreeLocation = i;
                         min = treePointers[i];
                         treePointers[i].setPrevious(null);
                         treePointers[i].setNext(null);
                    }
                    else {
                         setAfter(treePointers[lastTreeLocation], treePointers[i]);
                         treePointers[i].setNext(null);
                         lastTreeLocation = i;
                         if(min.getPriority() > treePointers[i].getPriority()) min = treePointers[i];
                    }
               }

          }

          size--;
          return value;

     }

     // sets newNext after elem
     private void setAfter(FibHeapElem elem, FibHeapElem newNext) {
          // System.out.println("Setting " + newNext.getPriority() + " after " + elem.getPriority());
          if (elem.hasNext()) {
               elem.getNext().setPrevious(newNext);
               newNext.setNext(elem.getNext());
          }
          elem.setNext(newNext);
          newNext.setPrevious(elem);
     }

     private void setBefore(FibHeapElem elem, FibHeapElem newPrevious) {
          if (elem.hasPrevious()) {
               elem.getPrevious().setNext(newPrevious);
               newPrevious.setPrevious(elem.getPrevious());
          }
          elem.setPrevious(newPrevious);
          newPrevious.setNext(elem);
     }

     private void removeElem(FibHeapElem elem) {
          removeElemList(elem);
          if(elem.hasChild()) {
               removeParent(elem.getChild());
          }
     }

     private void removeElemList(FibHeapElem elem) {
          if (elem.hasPrevious() && elem.hasNext()) {
               elem.getNext().setPrevious(elem.getPrevious());
               elem.getPrevious().setNext(elem.getNext());
          }
          else if (elem.hasPrevious()) {
               elem.getPrevious().setNext(null);
          }
          else if (elem.hasNext()) {
               elem.getNext().setPrevious(null);
          }

     }

     private void replaceNode(FibHeapElem oldElem, FibHeapElem newElem) {
          // System.out.println(oldElem.getPriority() + "  " + newElem.getPriority());
          if(newElem.isEqual(oldElem.getPrevious())) {
               if(oldElem.hasNext()) {
                    oldElem.getNext().setPrevious(newElem);
                    newElem.setNext(oldElem.getNext());
               }
               else {
                    newElem.setNext(null);

               }
          }
          else if(newElem.isEqual(oldElem.getNext())) {
               if(oldElem.hasPrevious()) {
                    oldElem.getPrevious().setNext(newElem);
                    newElem.setPrevious(oldElem.getPrevious());
               }
               else {
                    newElem.setNext(null);
               }
          }
          else if (oldElem.hasPrevious() && oldElem.hasNext()) {
               oldElem.getNext().setPrevious(newElem);
               newElem.setNext(oldElem.getNext());
               oldElem.getPrevious().setNext(newElem);
               newElem.setPrevious(oldElem.getPrevious());

          }
          else if (oldElem.hasPrevious()) {
               oldElem.getPrevious().setNext(newElem);
               newElem.setPrevious(oldElem.getPrevious());
               newElem.setNext(null);

          }
          else if (oldElem.hasNext()) {
               oldElem.getNext().setPrevious(newElem);
               newElem.setNext(oldElem.getNext());
               newElem.setPrevious(null);
          }
     }

     private int max(int num1, int num2) {
          if (num1 > num2) return num1;
          return num2;
     }

     private void removeParent(FibHeapElem elem) {
          elem.setParent(null);
          if (elem.hasNext()) {
               removeParent(elem.getNext());
          }
     }


     public void decreseKey(FibHeapElem elem, int newPrio) {
          elem.setPrio(newPrio);
          if (!(elem.hasParent()) && newPrio > min.getPriority()) return;
          if (!(elem.hasParent())) {
               min = elem;
               return;
          }

          if (elem.getParent().getPriority() <= newPrio) return;

          cutNode(elem);
     }

     private void cutNode(FibHeapElem elem) {
          // System.out.println("elem cut " + elem.getIdentifer());
          // printTree(min, 1);
          // System.out.println("-----");
          elem.unMarkNode();
          if (!(elem.hasParent())) return;



          if (!(elem.hasPrevious()) && elem.hasNext()) {
               elem.getParent().cutChild();
               // System.out.println(elem.getNext().getIdentifer());
          } else if (elem.hasPrevious()) {
               removeElemList(elem);
          } else {
               elem.getParent().removeChild();
          }

          elem.setNext(null);
          elem.setPrevious(null);


          placeNode(elem);

          elem.getParent().decreseTreeSize(elem.getSize());
          if (elem.getParent().isMarked()) {
               cutNode(elem.getParent());
               elem.setParent(null);
          }
          else {
               elem.getParent().markNode();
               elem.setParent(null);
          }
          // System.out.println("----");
          // printTree(min, 1);
          // System.out.println("----");


          // System.out.println("shit");
          // printTree(min, 1);
     }



     public void printTree(FibHeapElem x, int i) {
          while (x.hasPrevious()) {
               // System.out.println(x.getIdentifer());
               x = x.getPrevious();
          }

          System.out.println(x.getIdentifer() + " in layer " + i + " with prio " + x.getPriority());
          if (x.hasChild()) {
               printTree(x.getChild(), i+1);
          }

          while(x.hasNext()) {
               x = x.getNext();
               System.out.println(x.getIdentifer() + " in layer " + i + " with prio " + x.getPriority());
               if (x.hasChild()) {
                    printTree(x.getChild(), i+1);
               }
          }

     }

     public void printTree(int i) {
          FibHeapElem x = min;
          while (x.hasPrevious()) {
               x = x.getPrevious();
          }

          System.out.println(x.getIdentifer() + " in layer " + i + " with prio " + x.getPriority());

          if (x.hasChild()) {
               printTree(x.getChild(), i+1);
          }

          while(x.hasNext()) {
               x = x.getNext();
               System.out.println(x.getIdentifer() + " in layer " + i + " with prio " + x.getPriority());

               if (x.hasChild()) {
                    printTree(x.getChild(), i+1);
               }
          }

     }

     private void setChild(FibHeapElem parent, FibHeapElem child) {
          parent.setChild(child);
          child.setParent(parent);
     }

     public boolean isEmpty() {
          return (size == 0);
     }


}
