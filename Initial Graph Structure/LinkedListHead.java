class LinkedListHead {

     LinkedList head;
     int length;

     public LinkedListHead() {
          head = new LinkedList();
          length = 0;
     }

     public LinkedListHead(LinkedList head) {
          this.head = new LinkedList();
          length = 0;
          add(head);
     }

     public LinkedListHead(int vertex, int weight) {
          this.head = new LinkedList();
          length = 0;
          add(vertex, weight);
     }

     public void add(LinkedList next) {
          next.setNext(head);
          head = next;
          length++;
     }


     public void add(int vertex, int weight) {
          LinkedList next = new LinkedList(vertex, weight);
          next.setNext(head);
          head = next;
          length++;
     }

     public LinkedList getHead() {
          return head;
     }

     public int length() {
          return length;
     }




}
