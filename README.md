Welcome to my project.

I don't have much to show here.

Treewidth upper bound:

To run some treewidth upper bounding algorithms do the following:

Open up the shell and navigate to ParseOSM
    
    run the command : java treewidth

    It will prompt you to input a graph location. There are some already 
    available in Graphs folder so by typing .\Graphs\"City name".txt provided 
    such a graph exists this program will estimate it.

    It will then prompt you for a heuristic choice enter the corresponding 
    number 1, 2 or 3

    Enter the batch size (>0)

    It will then spew out a load of information about the iteration it and an 
    upper bound
    
    
If you wish to add your own map from Open Street Map:
    
    got to "https://www.openstreetmap.org/export"
    
    navigate to map you wish to create
    
    export it using Overpass API and damload file called map
    
    navigate to ParseOSM and run:
    java FileToGraph
    
    follow the instruction of the program
